package eu.openaire.api.repositories;

import eu.openaire.api.solr.SolrConnectionManager;
import eu.openaire.api.solr.SolrQueryParams;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.params.CursorMarkParams;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
@RequiredArgsConstructor
public class SolrRepository {

    private final SolrConnectionManager solrConnectionManager;
    private final Logger log = LogManager.getLogger(this.getClass());
    private static final String UNIQUE_KEY = "__indexrecordidentifier";

    public SolrDocument getById(String id) throws SolrServerException, IOException {
        return solrConnectionManager.getSolrClient().getById(id);
    }

    public QueryResponse query(SolrQueryParams queryParams) throws SolrServerException, IOException {
        SolrQuery query = new SolrQuery();
        query.setQuery(queryParams.getQueryString()); // add Q

        for (String fq : queryParams.getFilterQueries()) { // add FQ
            query.addFilterQuery(fq);
        }

        query.addField(queryParams.getFieldList()); // add FL

        // set pagination
        query.setRows(queryParams.getRows());
        String cursor = queryParams.getCursor();

        if (cursor != null && !cursor.isEmpty()) { // set cursor-based pagination
            query.set(CursorMarkParams.CURSOR_MARK_PARAM, cursor);
            query.addSort(UNIQUE_KEY, SolrQuery.ORDER.asc);
        } else { // set basic page/page-size pagination
            query.setStart(queryParams.getStart());
        }

        // set sorting
        for (var sortClause : queryParams.getSort()) {
            query.addSort(sortClause);
        }

        try {
            log.info(query);
            return solrConnectionManager.getSolrClient().query(query);
        } catch (SolrServerException e) {
            log.error(e.getMessage());
            throw new SolrServerException(e);
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new IOException(e);
        }
    }

    public SolrPingResponse ping() throws SolrServerException, IOException {
        return solrConnectionManager.ping();
    }
}
