package eu.openaire.api.solr;

import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;

@Data
public class SolrResponseBean {

	@Field("__json")
	private String jsonField;

}
