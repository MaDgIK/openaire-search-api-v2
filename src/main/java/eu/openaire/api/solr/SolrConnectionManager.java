package eu.openaire.api.solr;

import eu.openaire.api.config.properties.SolrProperties;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Scope("singleton")
public class SolrConnectionManager {

    @Getter
    private SolrClient solrClient;

    private final Logger log = LogManager.getLogger(this.getClass());

    private final SolrProperties solrProperties;

    @PostConstruct
    private void initSolrClient() throws SolrServerException, IOException {

        List<String> zkHosts = Arrays.asList(solrProperties.getZkHosts());

        log.info("Creating Solr client - Zookeeper instances: {}", zkHosts);
        log.info("Creating Solr client - Collection: {}", solrProperties.getCollection());

        // create SolrClient with ZooKeeper hosts
        solrClient = new CloudSolrClient.Builder(zkHosts, Optional.empty())
                .withConnectionTimeout(3000)
                .withSocketTimeout(30000)
                .build();

        ((CloudSolrClient) solrClient).setDefaultCollection(solrProperties.getCollection());
    }

    public SolrPingResponse ping() throws IOException, SolrServerException {
        return solrClient.ping();
    }

    @PreDestroy
    private void closeClient() throws IOException {
        log.info("Closing Solr client");

        if (solrClient != null) {
            solrClient.close();
        }
    }
}
