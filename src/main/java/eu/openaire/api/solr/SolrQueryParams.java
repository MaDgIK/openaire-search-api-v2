package eu.openaire.api.solr;

import lombok.Data;
import org.apache.solr.client.solrj.SolrQuery;
import java.util.List;

@Data
public class SolrQueryParams {
	private String queryString = "*:*";
	private List<String> filterQueries;
	private String fieldList = "__json";
	private Boolean debugQuery = false;
	private int start;
	private int rows;
	private List<SolrQuery.SortClause> sort;
	private String cursor;
}
