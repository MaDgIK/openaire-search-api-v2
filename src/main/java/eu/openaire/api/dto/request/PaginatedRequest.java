package eu.openaire.api.dto.request;

public interface PaginatedRequest {
	int getPage();
	int getPageSize();
	String getCursor();
}
