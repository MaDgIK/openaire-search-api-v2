package eu.openaire.api.dto.request.v1;

import eu.openaire.api.dto.request.PaginatedRequest;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import static eu.openaire.api.mappers.Utils.API_CURSOR_DESC;
import static eu.openaire.api.mappers.Utils.API_PAGE_DESC;
import static eu.openaire.api.mappers.Utils.API_PAGE_SIZE_DESC;

@Getter
@Setter
@Data
public class DataSourceRequest implements PaginatedRequest {

	@Parameter(
			description = "Keyword-based search",
			schema = @Schema(type = "string")
	)
	private String search;

	@Parameter(
			description = "The official name of the data source",
			schema = @Schema(type = "string")
	)
	private String[] officialName;

	@Parameter(
			description = "The English name of the data source",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] englishName;

	@Parameter(
			description = "The legal name of the organization in short form",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] legalShortName;

	@Parameter(
			description = "The OpenAIRE id of the data source",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] id;

	@Parameter(description = "The persistent identifier of the data source", array = @ArraySchema(schema = @Schema(type = "string")))
	private String[] pid;

	@Parameter(
			description = "List of subjects associated to the datasource",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] subjects;

	@Parameter(
			description = "The data source type; see all possible values <a href='https://api.openaire.eu/vocabularies/dnet:datasource_typologies' target='_blank'>here</a> ",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] dataSourceTypeName;

	@Parameter(
			description = "Types of content in the data source, as defined by OpenDOAR",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] contentTypes;

	@Parameter(
			description = "Retrieve data sources connected to the organization (with OpenAIRE id)",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relOrganizationId;

	@Parameter(
			description = "Retrieve data sources connected to the community (with OpenAIRE id)",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relCommunityId;

	@Parameter(
			description = "Retrieve data sources collected from the data source (with OpenAIRE id)",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relCollectedFromDatasourceId;

	@Min(value = 1)
	@Parameter(
			description = API_PAGE_DESC,
			schema = @Schema(defaultValue = "1", type = "integer")
	)
	private int page = 1;

	@Min(value = 1, message = "Page size must be at least 1")
	@Max(value = 100, message = "Page size must be at most 100")
	@Parameter(
			description = API_PAGE_SIZE_DESC,
			schema = @Schema(defaultValue = "10", type = "integer")
	)
	private int pageSize = 10;

	@Parameter(
			description = API_CURSOR_DESC,
			schema = @Schema(type = "string")
	)
	private String cursor;

	@Parameter(
			description = "The field to sort the results by and the sort direction. The format should be in the format `fieldname ASC|DESC`, organizations can be only sorted by the 'relevance'." ,
			schema = @Schema(defaultValue = "relevance DESC")
	)
	@Pattern(
			regexp = "^((relevance)\\s+(ASC|DESC),?\\s*)+$",
			message = "The field should be in the format 'fieldname ASC|DESC', organizations can be only sorted by the 'relevance'."
	)
	private String sortBy = "relevance DESC";

}