package eu.openaire.api.dto.request.validators;

import eu.openaire.api.dto.request.v1.ResearchProductsRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class ResearchProductsRequestValidator implements Validator {

	private final Map<String, Set<String>> allowableValues = new HashMap<>();
	{
		allowableValues.put("type", Set.of("publication", "dataset", "software", "other"));
		allowableValues.put("bestOpenAccessRightLabel", Set.of("OPEN SOURCE", "OPEN", "EMBARGO", "RESTRICTED", "CLOSED", "UNKNOWN"));
		allowableValues.put("impactClasses", Set.of("C1", "C2", "C3", "C4", "C5"));
		allowableValues.put("openAccessColor", Set.of("bronze", "gold", "hybrid"));
	}


	@Override
	public boolean supports(Class<?> clazz) {
		return ResearchProductsRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ResearchProductsRequest request = (ResearchProductsRequest) target;

		// Validate controlled fields
		Utils.validateStrArrayOneOf("type", request.getType(), allowableValues.get("type"), errors);
		Utils.validateStrArrayOneOf("bestOpenAccessRightLabel", request.getBestOpenAccessRightLabel(), allowableValues.get("bestOpenAccessRightLabel"), errors);

		Utils.validateStrArrayOneOf("influenceClass", request.getInfluenceClass(), allowableValues.get("impactClasses"), errors);
		Utils.validateStrArrayOneOf("popularityClass", request.getPopularityClass(), allowableValues.get("impactClasses"), errors);
		Utils.validateStrArrayOneOf("impulseClass", request.getImpulseClass(), allowableValues.get("impactClasses"), errors);
		Utils.validateStrArrayOneOf("citationCountClass", request.getCitationCountClass(), allowableValues.get("impactClasses"), errors);

		Utils.validateIntArrayInRange("sdg", request.getSdg(), 1, 17, errors);
		Utils.validateStrArrayOneOf("openAccessColor", request.getOpenAccessColor(), allowableValues.get("openAccessColor"), errors);

	}


}
