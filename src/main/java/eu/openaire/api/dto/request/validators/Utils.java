package eu.openaire.api.dto.request.validators;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Utils {

	public static void validateStrArrayOneOf(String paramName, String[] paramValues, Set<String> paramAllowableValues, Errors errors) {
		if (paramValues == null) {
			return;
		}

		if (paramAllowableValues == null || paramAllowableValues.isEmpty()) {
			errors.rejectValue(paramName, paramName + ".notConfigured", "Allowable types for " + paramName + " are not configured.");
			return;
		}

		for (String value : paramValues) {
			if (!paramAllowableValues.contains(value)) {
				errors.rejectValue(paramName, paramName + ".invalid", "Invalid '" + paramName + "' property value given: '" + value + "' - valid values are: " + paramAllowableValues);
			}
		}
	}

	public static void validateIntArrayInRange(String paramName, Integer[] paramValues, Integer minValue, Integer maxValue, Errors errors) {
		if (paramValues == null) {
			return;
		}

		for (Integer value : paramValues) {
			if (value < minValue || value > maxValue) {
				errors.rejectValue(paramName, paramName + ".invalid", "Invalid '" + paramName + "' property value given: '" + value + "' - valid values are: [" + minValue + ", " + maxValue + "]");
			}
		}
	}

	public static String getErrorMessage(List<ObjectError> allErrors) {
		return allErrors.stream()
				.map(DefaultMessageSourceResolvable::getDefaultMessage)
				.collect(Collectors.joining("\n"));
	}

}
