package eu.openaire.api.dto.request.validators;

import eu.openaire.api.dto.request.v1.DataSourceRequest;
import eu.openaire.api.dto.request.v1.OrganizationRequest;
import eu.openaire.api.dto.request.v1.ProjectRequest;
import eu.openaire.api.dto.request.v1.ResearchProductsRequest;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.lang.reflect.Field;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class AllowableFieldsValidator implements Validator {

	private final HttpServletRequest request;

	public AllowableFieldsValidator(HttpServletRequest request) {
		this.request = request;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		// Add any classes you want this validator to support
		return ResearchProductsRequest.class.isAssignableFrom(clazz)
				|| OrganizationRequest.class.isAssignableFrom(clazz)
				|| DataSourceRequest.class.isAssignableFrom(clazz)
				|| ProjectRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Set<String> validParams = Stream.of(target.getClass().getDeclaredFields())
				.map(Field::getName)
				.collect(Collectors.toSet());

		// reject any parameters that are not in the allowed request object
		request.getParameterMap().forEach((key, value) -> {
			if (!validParams.contains(key)) {
				errors.reject(key, "Unknown parameter: " + key + "; valid parameters are: " + validParams);
			}
		});
	}

}
