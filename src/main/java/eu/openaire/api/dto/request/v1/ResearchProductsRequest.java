package eu.openaire.api.dto.request.v1;

import eu.openaire.api.dto.request.PaginatedRequest;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import static eu.openaire.api.mappers.Utils.API_CURSOR_DESC;
import static eu.openaire.api.mappers.Utils.API_PAGE_DESC;
import static eu.openaire.api.mappers.Utils.API_PAGE_SIZE_DESC;

@Getter
@Setter
@Data
public class ResearchProductsRequest implements PaginatedRequest {

	@Parameter(
			description = "Keyword-based search",
			schema = @Schema(type = "string")
	)
	private String search;

	@Parameter(
			description = "Search in the research product's main title",
			schema = @Schema(type = "string")
	)
	private String mainTitle;

	@Parameter(
			description = "Search in the research product's description",
			schema = @Schema(type = "string")
	)
	private String description;

	@Parameter(
			description = "The OpenAIRE id of the research product",
			array = @ArraySchema(
					schema = @Schema(type = "string")
			)
	)
	private String[] id;

	@Parameter(
			description = "The persistent identifier of the research product",
			array = @ArraySchema(
					schema = @Schema(type = "string")
			)
	)
	private String[] pid;

	@Parameter(
			description = "The identifier of the record at the original sources",
			array = @ArraySchema(
					schema = @Schema(type = "string")
			)
	)
	private String[] originalId;

	@Parameter(
			description = "The type of the research product",
			array = @ArraySchema(
					schema = @Schema(
							type = "string",
							allowableValues = {"publication", "dataset", "software", "other"}
					)
			)
	)
	private String[] type;

	@Parameter(
			description = "Gets the research products whose publication date is greater than or equal to he given date. Provide a date in YYYY or YYYY-MM-DD format",
			schema = @Schema(type = "string"))
	private String fromPublicationDate;

	@Parameter(
			description = "Gets the research products whose publication date is less than or equal to the given date. Provide a date in YYYY or YYYY-MM-DD format",
			schema = @Schema(type = "string"))
	private String toPublicationDate;

	@Parameter(
			description = "List of subjects associated to the research product",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] subjects;

	@Parameter(
			description = "The country code for the country associated with the research product",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] countryCode;

	@Parameter(
			description = "The full name of the authors involved in producing this research product",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] authorFullName;

	@Parameter(
			description = "The ORCiD of the authors involved in producing this research product",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] authorOrcid;

	@Parameter(
			description = "The name of the entity that holds, archives, publishes prints, distributes, releases, issues, or produces the resource.",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] publisher;

	@Parameter(
			description = "The best open access rights among the research product's instances",
			array = @ArraySchema(
					schema = @Schema(
							type = "string",
							allowableValues = {"OPEN SOURCE", "OPEN", "EMBARGO", "RESTRICTED", "CLOSED", "UNKNOWN"}
					)
			)
	)
	private String[] bestOpenAccessRightLabel;

	@Parameter(
			description = "Citation-based indicator that reflects the overall impact of a research product; please choose a class among 'C1', 'C2', 'C3', 'C4', 'C5' for  top 0.01%, top 0.1%, top 1%, top 10%, and average in terms of influence respectively",
			array = @ArraySchema(
					schema = @Schema(
							type = "string",
							allowableValues = {"C1", "C2", "C3", "C4", "C5"}
					)
			)
	)
	private String[] influenceClass;

	@Parameter(
			description = "Citation-based indicator that reflects current impact or attention of a research product; please choose a class among 'C1', 'C2', 'C3', 'C4', 'C5' for  top 0.01%, top 0.1%, top 1%, top 10%, and average in terms of popularity respectively",
			array = @ArraySchema(
					schema = @Schema(
							type = "string",
							allowableValues = {"C1", "C2", "C3", "C4", "C5"}
					)
			)
	)
	private String[] popularityClass;

	@Parameter(
			description = "Citation-based indicator that reflects the initial momentum of a research product directly after its publication; please choose a class among 'C1', 'C2', 'C3', 'C4', 'C5' for  top 0.01%, top 0.1%, top 1%, top 10%, and in terms of average impulse respectively",
			array = @ArraySchema(
					schema = @Schema(
							type = "string",
							allowableValues = {"C1", "C2", "C3", "C4", "C5"}
					)
			)
	)
	private String[] impulseClass;

	@Parameter(
			description = "Citation-based indicator that reflects the overall impact of a research product by summing all its citations; please choose a class among 'C1', 'C2', 'C3', 'C4', 'C5' for  top 0.01%, top 0.1%, top 1%, top 10%, and average in terms of citation count respectively",
			array = @ArraySchema(
					schema = @Schema(
							type = "string",
							allowableValues = {"C1", "C2", "C3", "C4", "C5"}
					)
			)
	)
	private String[] citationCountClass;

	// TODO: should we validate the instance type values?
	@Parameter(
			description = "Retrieve publications of the given instance type; check <a href='http://api.openaire.eu/vocabularies/dnet:publication_resource' target='_blank'>here</a> for all possible instance type values `[Only for publications]`",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] instanceType;

	// publication related filters
	@Parameter(
			description = "Retrieves publications classified with the respective Sustainable Development Goal number (for further information check <a href='https://sdgs.un.org/goals' target='_blank'>here</a>); please provide an SDG number between 1 and 17 `[Only for publications]`",
			array = @ArraySchema(
					schema = @Schema(
							type = "integer",
							minimum = "1",
							maximum = "17"
					)
			)
	)
	private Integer[] sdg;

	@Parameter(
			description = "Retrieves publications classified with a given Field of Science (FOS); please provide a valid <a href='https://explore.openaire.eu/assets/common-assets/vocabulary/fos.json' target='_blank'>FOS classification identifier</a>  `[Only for publications]`",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] fos;


	@Parameter(
			description = "Indicates whether the publications are peerReviewed or not `[Only for publications]`",
			schema = @Schema(type = "boolean")
	)
	private Boolean isPeerReviewed;

	@Parameter(
			description = "Indicates whether the publication was published in a diamond journal or not `[Only for publications]`",
			schema = @Schema(type = "boolean")
	)
	private Boolean isInDiamondJournal;

	@Parameter(
			description = "Indicates whether the publication was publicly funded or not `[Only for publications]`",
			schema = @Schema(type = "boolean")
	)
	private Boolean isPubliclyFunded;

	@Parameter(
			description = "Indicates whether the publication was published following the green open access model `[Only for publications]`",
			schema = @Schema(type = "boolean")
	)
	private Boolean isGreen;

	@Parameter(
			description = "Specifies the Open Access color of the publication `[Only for publications]`",
			array = @ArraySchema(
					schema = @Schema(
							type = "string", allowableValues = {"bronze", "gold", "hybrid"}
					)
			)
	)
	private String[] openAccessColor;

	// related object filters
	@Parameter(
			description = "Retrieve research products connected to the organization (with OpenAIRE id)",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relOrganizationId;

	@Parameter(
			description = "Retrieve research products connected to the community (with OpenAIRE id)",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relCommunityId;

	@Parameter(
			description = "Retrieve research products connected to the project (with OpenAIRE id)",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relProjectId;

	@Parameter(
			description = "Retrieve research products connected to the project with code",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relProjectCode;

	@Parameter(
			description = "Retrieve research products that are connected to a project",
			schema = @Schema(type = "boolean")
	)
	private Boolean hasProjectRel;

	@Parameter(
			description = "Retrieve research products connected to a project that has a funder with the given short name",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relProjectFundingShortName;

	@Parameter(
			description = "Retrieve research products connected to a project that has the given funding identifier",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relProjectFundingStreamId;

	@Parameter(
			description = "Retrieve research products hosted by the data source (with OpenAIRE id)",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relHostingDataSourceId;

	@Parameter(
			description = "Retrieve research products collected from the data source (with OpenAIRE id)",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relCollectedFromDatasourceId;

	@Min(value = 1)
	@Parameter(
			description = API_PAGE_DESC,
			schema = @Schema(defaultValue = "1", type = "integer")
	)
	private int page = 1;

	@Min(value = 1, message = "Page size must be at least 1")
	@Max(value = 100, message = "Page size must be at most 100")
	@Parameter(
			description = API_PAGE_SIZE_DESC,
			schema = @Schema(defaultValue = "10", type = "integer")
	)
	private int pageSize = 10;

	@Parameter(
			description = API_CURSOR_DESC,
			schema = @Schema(type = "string")
	)
	private String cursor;

	@Parameter(
			description = "The field to sort the results by and the sort direction. The format should be in the format `fieldname ASC|DESC`, where fieldname is one of 'relevance', 'publicationDate', 'dateOfCollection', 'influence', 'popularity', 'citationCount', 'impulse'. Multiple sorting parameters should be comma-separated.",
			schema = @Schema(defaultValue = "relevance DESC")
	)
	@Pattern(
			regexp = "^((relevance|publicationDate|dateOfCollection|influence|popularity|citationCount|impulse)\\s+(ASC|DESC),?\\s*)+$",
			message = "The field should be in the format 'fieldname ASC|DESC', research products can be only sorted by the 'relevance', 'publicationDate', 'dateOfCollection', 'influence', 'citationCount', 'impulse'."
	)
	private String sortBy = "relevance DESC";

}