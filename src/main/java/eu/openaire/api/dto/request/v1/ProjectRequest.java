package eu.openaire.api.dto.request.v1;

import eu.openaire.api.dto.request.PaginatedRequest;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import static eu.openaire.api.mappers.Utils.API_CURSOR_DESC;
import static eu.openaire.api.mappers.Utils.API_PAGE_DESC;
import static eu.openaire.api.mappers.Utils.API_PAGE_SIZE_DESC;

@Getter
@Setter
@Data
public class ProjectRequest implements PaginatedRequest {

	@Parameter(
			description = "Keyword-based search",
			schema = @Schema(type = "string")
	)
	private String search;

	@Parameter(
			description = "Search in the project's title",
			schema = @Schema(type = "string")
	)
	private String title;

	@Parameter(
			description = "The project's keywords",
			schema = @Schema(type = "string")
	)
	private String keywords;

	@Parameter(
			description = "The OpenAIRE id of the project",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] id;

	@Parameter(
			description = "The grant agreement (GA) code of the project",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] code;

	@Parameter(
			description = "Project's acronym",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] acronym;

	@Parameter(
			description = "The identifier of the research call",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] callIdentifier;

	@Parameter(
			description = "The short name of the funder",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] fundingShortName;

	@Parameter(
			description = "The identifier of the funding stream",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] fundingStreamId;

	@Parameter(
			description = "Gets the projects with start date greater than or equal to the given date. Provide a date in YYYY or YYYY-MM-DD format",
			schema = @Schema(type = "string")
	)
	private String fromStartDate;

	@Parameter(
			description = "Gets the projects with start date less than or equal to the given date. Provide a date in YYYY or YYYY-MM-DD format",
			schema = @Schema(type = "string")
	)
	private String toStartDate;

	@Parameter(
			description = "Gets the projects with end date greater than or equal to the given date. Provide a date in YYYY or YYYY-MM-DD format",
			schema = @Schema(type = "string")
	)
	private String fromEndDate;

	@Parameter(
			description = "Gets the projects with end date less than or equal to the given date. Provide a date in YYYY or YYYY-MM-DD format",
			schema = @Schema(type = "string")
	)
	private String toEndDate;

	@Parameter(
			description = "The name or short name of the related organization",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relOrganizationName;

	@Parameter(
			description = "The organization identifier of the related organization",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relOrganizationId;

	@Parameter(
			description = "Retrieve projects connected to the community (with OpenAIRE id)",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relCommunityId;

	@Parameter(
			description = "The country code of the related organizations",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relOrganizationCountryCode;

	@Parameter(
			description = "Retrieve projects collected from the data source (with OpenAIRE id)",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relCollectedFromDatasourceId;

	@Min(value = 1)
	@Parameter(
			description = API_PAGE_DESC,
			schema = @Schema(defaultValue = "1", type = "integer"))
	private int page = 1;

	@Min(value = 1, message = "Page size must be at least 1")
	@Max(value = 100, message = "Page size must be at most 100")
	@Parameter(
			description = API_PAGE_SIZE_DESC,
			schema = @Schema(defaultValue = "10", type = "integer"))
	private int pageSize = 10;

	@Parameter(
			description = API_CURSOR_DESC,
			schema = @Schema(type = "string"))
	private String cursor;

	@Parameter(
			description = "The field to sort the results by and the sort direction. The format should be in the format `fieldname ASC|DESC`, where fieldname is one of 'relevance', 'startDate', 'endDate'. Multiple sorting parameters should be comma-separated." ,
			schema = @Schema(defaultValue = "relevance DESC")
	)
	@Pattern(
			regexp = "^((relevance|startDate|endDate)\\s+(ASC|DESC),?\\s*)+$",
			message = "The field should be in the format 'fieldname ASC|DESC', where fieldname is one of 'relevance', 'startDate', 'endDate'. Multiple sorting parameters should be comma-separated."
	)
	private String sortBy = "relevance DESC";

}