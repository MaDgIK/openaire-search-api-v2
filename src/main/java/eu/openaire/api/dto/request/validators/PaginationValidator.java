package eu.openaire.api.dto.request.validators;

import eu.openaire.api.dto.request.v1.DataSourceRequest;
import eu.openaire.api.dto.request.v1.OrganizationRequest;
import eu.openaire.api.dto.request.PaginatedRequest;
import eu.openaire.api.dto.request.v1.ProjectRequest;
import eu.openaire.api.dto.request.v1.ResearchProductsRequest;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class PaginationValidator implements Validator {

	private final HttpServletRequest request;

	private static final int MAX_RESULTS = 10000;

	public PaginationValidator(HttpServletRequest request) {
		this.request = request;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		// Add any classes you want this validator to support
		return ResearchProductsRequest.class.isAssignableFrom(clazz)
				|| OrganizationRequest.class.isAssignableFrom(clazz)
				|| DataSourceRequest.class.isAssignableFrom(clazz)
				|| ProjectRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		PaginatedRequest paginatedRequest = (PaginatedRequest) target;

		if (paginatedRequest.getPage() * paginatedRequest.getPageSize() > MAX_RESULTS) {
			errors.reject("page", "Page * pageSize must be less than or equal to " + MAX_RESULTS);
		}

	}

}
