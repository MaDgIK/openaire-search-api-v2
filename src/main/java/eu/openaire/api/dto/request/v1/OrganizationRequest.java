package eu.openaire.api.dto.request.v1;

import eu.openaire.api.dto.request.PaginatedRequest;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import static eu.openaire.api.mappers.Utils.API_CURSOR_DESC;
import static eu.openaire.api.mappers.Utils.API_PAGE_DESC;
import static eu.openaire.api.mappers.Utils.API_PAGE_SIZE_DESC;

@Getter
@Setter
@Data
public class OrganizationRequest implements PaginatedRequest {

	@Parameter(
			description = "Keyword-based search",
			schema = @Schema(type = "string")
	)
	private String search;

	@Parameter(
			description = "The legal name of the organization",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] legalName;

	@Parameter(
			description = "The legal name of the organization in short form",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] legalShortName;

	@Parameter(
			description = "The OpenAIRE id of the organization",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] id;

	@Parameter(
			description = "The persistent identifier of the organization",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] pid;

	@Parameter(
			description = "The country code of the organization",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] countryCode;

	@Parameter(
			description = "Retrieve organizations connected to the community (with OpenAIRE id)",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relCommunityId;

	@Parameter(
			description = "Retrieve organizations collected from the data source (with OpenAIRE id)",
			array = @ArraySchema(schema = @Schema(type = "string"))
	)
	private String[] relCollectedFromDatasourceId;

	@Min(value = 1)
	@Parameter(
			description = API_PAGE_DESC,
			schema = @Schema(defaultValue = "1", type = "integer"))
	private int page = 1;

	@Min(value = 1, message = "Page size must be at least 1")
	@Max(value = 100, message = "Page size must be at most 100")
	@Parameter(
			description = API_PAGE_SIZE_DESC,
			schema = @Schema(defaultValue = "10", type = "integer"))
	private int pageSize = 10;

	@Parameter(
			description = API_CURSOR_DESC,
			schema = @Schema(type = "string"))
	private String cursor;

	@Parameter(
			description = "The field to sort the results by and the sort direction. The format should be in the format `fieldname ASC|DESC`, organizations can be only sorted by the 'relevance'." ,
			schema = @Schema(defaultValue = "relevance DESC")
	)
	@Pattern(
			regexp = "^((relevance)\\s+(ASC|DESC),?\\s*)+$",
			message = "The field should be in the format 'fieldname ASC|DESC', organizations can be only sorted by the 'relevance'."
	)
	private String sortBy = "relevance DESC";

}