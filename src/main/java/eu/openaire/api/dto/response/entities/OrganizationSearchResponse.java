package eu.openaire.api.dto.response.entities;

import eu.dnetlib.dhp.oa.model.graph.Organization;
import eu.openaire.api.dto.response.SearchResponse;

// used only to display the response type in the swagger documentation
public class OrganizationSearchResponse extends SearchResponse<Organization> {
}
