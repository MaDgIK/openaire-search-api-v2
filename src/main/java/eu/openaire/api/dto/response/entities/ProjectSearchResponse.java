package eu.openaire.api.dto.response.entities;

import eu.dnetlib.dhp.oa.model.graph.Project;
import eu.openaire.api.dto.response.SearchResponse;

// used only to display the response type in the swagger documentation
public class ProjectSearchResponse extends SearchResponse<Project> {
}
