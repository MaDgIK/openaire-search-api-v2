package eu.openaire.api.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import static eu.openaire.api.mappers.Utils.API_NEXT_CURSOR_DESC;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchHeader {
	private SearchHeaderDebug debug;
	private Long numFound;
	private Float maxScore;
	private Integer queryTime;
	private Integer page;
	private Integer pageSize;

	@Schema(description = API_NEXT_CURSOR_DESC)
	private String nextCursor;
}
