package eu.openaire.api.dto.response;

import eu.openaire.api.solr.SolrQueryParams;
import lombok.Data;

@Data
public class SearchHeaderDebug {

	private SolrQueryParams queryParams;

	private String parsedQuery;

	private String parsedFilterQuery;

}
