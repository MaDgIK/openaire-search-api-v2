package eu.openaire.api.errors;

import eu.openaire.api.errors.exceptions.BadRequestException;
import eu.openaire.api.errors.exceptions.NotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ServiceExceptionHandler {

	private final Logger log = LogManager.getLogger(this.getClass());

	private static final String URL_REGEX = "https?://\\S*";
	private static final Pattern URL_PATTERN = Pattern.compile(URL_REGEX);

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<ErrorResponse> handleNotFoundException(NotFoundException e, WebRequest request) {
		return this.handleException(e.getMessage(), request, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<ErrorResponse> handleBadRequestException(BadRequestException e, WebRequest request) {
		return this.handleException(e.getMessage(), request, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorResponse> handleArgumentNotValidException(MethodArgumentNotValidException e, WebRequest request) {

		// join all validation errors into a single message
		String message = e.getFieldErrors()
			.stream()
			.map(error -> error.getField() + ": " + error.getDefaultMessage())
			.collect(Collectors.joining("; "));

		return this.handleException(message, request, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NoResourceFoundException.class)
	public ResponseEntity<ErrorResponse> handleNoResourceFoundExceptions(NoResourceFoundException e, WebRequest request) {
		return this.handleException(e.getMessage(), request, HttpStatus.METHOD_NOT_ALLOWED);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> handleAllOtherExceptions(Exception e, WebRequest request) {
		//todo: log4j2.xml - add error appender
		e.printStackTrace();
		return this.handleException(e.getMessage(), request, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(RestClientResponseException.class)
	public ResponseEntity<ErrorResponse> handleRestClientResponseException(RestClientResponseException e, WebRequest request) {
		HttpStatusCode status = e.getStatusCode();
		String path = ((ServletWebRequest) request).getRequest().getRequestURI();

		ErrorResponse response = ErrorResponse.builder()
				.message(e.getMessage())
				.error(status.toString())
				.code(status.value())
				.timestamp(new Date())
				.path(path)
				.build();

		return ResponseEntity.status(status.value()).body(response);
	}

	private ResponseEntity<ErrorResponse> handleException(String message, WebRequest request, HttpStatus httpStatus) {
		var req = ((ServletWebRequest)request).getRequest();
		String path = String.format("%s?%s", req.getRequestURI(), req.getQueryString());

		ErrorResponse response = ErrorResponse.builder()
			.message(obfuscateUrlsInMessage(message))
			.error(httpStatus.getReasonPhrase())
			.code(httpStatus.value())
			.timestamp(new Date())
			.path(path)
			.build();

		return ResponseEntity
			.status(httpStatus)
			.body(response);
	}

	private static String obfuscateUrlsInMessage(String message) {
		Matcher matcher = URL_PATTERN.matcher(message);
		return matcher.replaceAll("[https://***].");
	}
}