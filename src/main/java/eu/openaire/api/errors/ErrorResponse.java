package eu.openaire.api.errors;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class ErrorResponse {

	@Schema(description = "The application error message", type = "string")
	private String message;

	@Schema(description = "The http error message", type = "string")
	private String error;

	@Schema(description = "The http error code", type = "integer")
	private int code;

	@Schema(description = "The timestamp the error occurred", type = "date")
	private Date timestamp;

	@Schema(description = "The path of the request", type = "string")
	private String path;

}
