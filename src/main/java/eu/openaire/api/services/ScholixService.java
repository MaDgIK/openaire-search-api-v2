package eu.openaire.api.services;

import eu.dnetlib.dhp.schema.sx.api.model.v2.PageResultType;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

@Service
@RequiredArgsConstructor
public class ScholixService {

	private final RestClient restClient;
	private final Logger log = LogManager.getLogger(this.getClass());
	private static final String OPENAIRE_ID_PREFIX = "50|";

	@Timed
	public PageResultType getLinks(String sourcePid,
								   String targetPid,
								   String targetPublisher,
								   String targetType,
								   String sourcePublisher,
								   String sourceType,
								   String relation,
								   String relationDate,
								   int page) {
		try {
			if (!sourcePid.startsWith(OPENAIRE_ID_PREFIX)) {
				sourcePid = OPENAIRE_ID_PREFIX + sourcePid;
			}

			String finalSourcePid = sourcePid;

            return restClient.get()
					.uri(uriBuilder -> uriBuilder
							.path("/v3/Links")
							.queryParam("sourcePid", finalSourcePid)
							.queryParam("targetPid", targetPid)
							.queryParam("targetPublisher", targetPublisher)
							.queryParam("targetType", targetType)
							.queryParam("sourcePublisher", sourcePublisher)
							.queryParam("sourceType", sourceType)
							.queryParam("relation", relation)
							.queryParam("relationDate", relationDate)
							.queryParam("page", page)
							.build())
					.retrieve()
					.body(PageResultType.class);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new RuntimeException("Unexpected error: " + e.getMessage(), e);
		}
	}
}
