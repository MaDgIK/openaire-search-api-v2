package eu.openaire.api.services;

import eu.dnetlib.dhp.oa.model.graph.GraphResult;
import eu.openaire.api.dto.request.v1.ResearchProductsRequest;
import eu.openaire.api.dto.response.SearchHeader;
import eu.openaire.api.dto.response.SearchResponse;
import eu.openaire.api.errors.exceptions.NotFoundException;
import eu.openaire.api.mappers.query.ResearchProductsRequestMapper;
import eu.openaire.api.mappers.response.ResponseHeaderMapper;
import eu.openaire.api.mappers.response.ResponseResultsMapper;
import eu.openaire.api.mappers.response.entities.ResearchProductMapper;
import eu.openaire.api.repositories.SolrRepository;
import eu.openaire.api.solr.SolrQueryParams;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ResearchProductService {

	private final SolrRepository solrRepository;

	private final ResearchProductsRequestMapper researchProductsRequestMapper;

	private final ResponseHeaderMapper responseHeaderMapper;

	private final ResponseResultsMapper responseResultsMapper;

	private final ResearchProductMapper researchProductMapper;

	private final Logger log = LogManager.getLogger(this.getClass());

	@SneakyThrows
	@Timed
	public GraphResult getById(String id) {

		var doc = solrRepository.getById(id);
		if (doc == null) {
			throw new NotFoundException("Research product with id: " + id + " not found");
		}

		return responseResultsMapper.toSingleResult(doc.get("__json").toString(), researchProductMapper::toGraphResult);

	}

	@SneakyThrows
	@Timed
	public SearchResponse<GraphResult> search(ResearchProductsRequest request) {

		SolrQueryParams solrQueryParams = researchProductsRequestMapper.toSolrQuery(request);

		log.debug(solrQueryParams);

		// perform the query
		QueryResponse queryResponse = solrRepository.query(solrQueryParams);

		// format the header response
		SearchHeader searchHeader = responseHeaderMapper.toSearchHeader(queryResponse, solrQueryParams, request.getPage(), request.getPageSize());

		// format the result documents
		List<GraphResult> results = responseResultsMapper.toSearchResults(queryResponse, researchProductMapper::toGraphResult);

		return SearchResponse.<GraphResult>builder()
				.header(searchHeader)
				.results(results)
				.build();

	}
}
