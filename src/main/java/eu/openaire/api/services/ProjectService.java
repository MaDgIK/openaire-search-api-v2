package eu.openaire.api.services;

import eu.dnetlib.dhp.oa.model.graph.Project;
import eu.openaire.api.dto.request.v1.ProjectRequest;
import eu.openaire.api.dto.response.SearchHeader;
import eu.openaire.api.dto.response.SearchResponse;
import eu.openaire.api.errors.exceptions.NotFoundException;
import eu.openaire.api.mappers.query.ProjectRequestMapper;
import eu.openaire.api.mappers.response.ResponseHeaderMapper;
import eu.openaire.api.mappers.response.ResponseResultsMapper;
import eu.openaire.api.mappers.response.entities.ProjectMapper;
import eu.openaire.api.repositories.SolrRepository;
import eu.openaire.api.solr.SolrQueryParams;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectService {

    private final SolrRepository solrRepository;

    private final ProjectRequestMapper projectRequestMapper;

    private final ResponseHeaderMapper responseHeaderMapper;

    private final ResponseResultsMapper responseResultsMapper;

    private final ProjectMapper projectMapper;

    private final Logger log = LogManager.getLogger(this.getClass());

    @SneakyThrows
    @Timed
    public Project getById(String id) {

        var doc = solrRepository.getById(id);
        if (doc == null) {
            throw new NotFoundException("Project with id: " + id + " not found");
        }

        return responseResultsMapper.toSingleResult(doc.get("__json").toString(), projectMapper::toGraphProject);
    }

    @SneakyThrows
    @Timed
    public SearchResponse<Project> search(ProjectRequest request) {

        SolrQueryParams solrQueryParams = projectRequestMapper.toSolrQuery(request);

        log.debug(solrQueryParams);

        // perform the query
        QueryResponse queryResponse = solrRepository.query(solrQueryParams);

        // format the header response
        SearchHeader searchHeader = responseHeaderMapper.toSearchHeader(queryResponse, solrQueryParams, request.getPage(), request.getPageSize());

        // format the result documents
        List<Project> results = responseResultsMapper.toSearchResults(queryResponse, projectMapper::toGraphProject);

        return SearchResponse.<Project>builder()
            .header(searchHeader)
            .results(results)
            .build();

    }
}
