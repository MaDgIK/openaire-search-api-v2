package eu.openaire.api.services;

import eu.dnetlib.dhp.oa.model.graph.Organization;
import eu.openaire.api.dto.request.v1.OrganizationRequest;
import eu.openaire.api.dto.response.SearchHeader;
import eu.openaire.api.dto.response.SearchResponse;
import eu.openaire.api.errors.exceptions.NotFoundException;
import eu.openaire.api.mappers.query.OrganizationRequestMapper;
import eu.openaire.api.mappers.response.ResponseHeaderMapper;
import eu.openaire.api.mappers.response.ResponseResultsMapper;
import eu.openaire.api.mappers.response.entities.OrganizationMapper;
import eu.openaire.api.repositories.SolrRepository;
import eu.openaire.api.solr.SolrQueryParams;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrganizationService {

	private final SolrRepository solrRepository;

	private final OrganizationRequestMapper organizationRequestMapper;

	private final ResponseHeaderMapper responseHeaderMapper;

	private final ResponseResultsMapper responseResultsMapper;

	private final OrganizationMapper organizationMapper;

	private final Logger log = LogManager.getLogger(this.getClass());

	@SneakyThrows
	@Timed
	public Organization getById(String id) {

		var doc = solrRepository.getById(id);
		if (doc == null) {
			throw new NotFoundException("Organization with id: " + id + " not found");
		}

		return responseResultsMapper.toSingleResult(doc.get("__json").toString(), organizationMapper::toGraphOrganization);
	}

	@SneakyThrows
	@Timed
	public SearchResponse<Organization> search(OrganizationRequest request) {

		SolrQueryParams solrQueryParams = organizationRequestMapper.toSolrQuery(request);

		log.debug(solrQueryParams);

		// format the header response
		QueryResponse queryResponse = solrRepository.query(solrQueryParams);

		// format the result documents
		SearchHeader searchHeader = responseHeaderMapper.toSearchHeader(queryResponse, solrQueryParams, request.getPage(), request.getPageSize());

		// format the result documents
		List<Organization> results = responseResultsMapper.toSearchResults(queryResponse, organizationMapper::toGraphOrganization);

		return SearchResponse.<Organization>builder()
				.header(searchHeader)
				.results(results)
				.build();

	}
}
