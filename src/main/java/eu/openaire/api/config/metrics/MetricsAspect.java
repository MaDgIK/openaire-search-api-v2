package eu.openaire.api.config.metrics;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Component
public class MetricsAspect {

    private final MeterRegistry meterRegistry;

    @Autowired
    public MetricsAspect(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    /***
     * Advice for timing methods annotated with {@link Timed}.
     */
    @Around("@annotation(io.micrometer.core.annotation.Timed)")
    public Object timeAnnotatedMethods(ProceedingJoinPoint joinPoint) throws Throwable {
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();

        String className = joinPoint.getTarget().getClass().getSimpleName();
        String methodName = method.getName();
        String timerName = className + "." + methodName;

        Timer.Sample sample = Timer.start(meterRegistry);

        try {
            return joinPoint.proceed();
        } finally {
            sample.stop(Timer.builder(timerName)
                    .tags("application", "openaire-search-api")
                    .tags("class", className)
                    .tags("method", methodName)
                    .publishPercentiles(0.5, 0.95)
                    .register(meterRegistry));
        }
    }
}
