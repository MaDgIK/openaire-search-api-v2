package eu.openaire.api.config.metrics;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MetricsConfig {
    private final MeterRegistry registry;
    private final String appName;
    private final String appVersion;

    public MetricsConfig(MeterRegistry registry,
                         @Value("${app.name}") String appName,
                         @Value("${app.version}") String appVersion) {
        this.registry = registry;
        this.appName = appName;
        this.appVersion = appVersion;
    }

    @PostConstruct
    public void micrometerInfo() {
        Gauge.builder("micrometer.info", () -> 1.0) // "." will be converted to "_"
                .tags("component", appName, "scmTag", "HEAD", "version", appVersion)
                .description("OpenAIRE Graph Search API")
                .register(registry);
    }
}


