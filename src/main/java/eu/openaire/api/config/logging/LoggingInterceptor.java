package eu.openaire.api.config.logging;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;

public class LoggingInterceptor implements HandlerInterceptor {

	private final Logger logger = LogManager.getLogger(this.getClass());

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//		logger.info("Received request: {} {}?{} from {}", request.getMethod(), request.getRequestURI(), request.getQueryString(), request.getRemoteAddr());
		request.setAttribute("startTime", System.currentTimeMillis());
		return true;
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		long startTime = (long) request.getAttribute("startTime");
		logger.info("Request: {} {}?{} {} {}", request.getMethod(), request.getRequestURI(), request.getQueryString(), response.getStatus(), System.currentTimeMillis() - startTime);
	}
}