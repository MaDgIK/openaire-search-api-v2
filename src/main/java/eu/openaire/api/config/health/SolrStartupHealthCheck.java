package eu.openaire.api.config.health;

import eu.openaire.api.repositories.SolrRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class SolrStartupHealthCheck implements ApplicationRunner {
	private final Logger log = LogManager.getLogger(this.getClass());

	@Autowired
	private SolrRepository solrRepository;

	@Override
	public void run(ApplicationArguments args) throws RuntimeException {
		try {
			if (solrRepository.ping().getStatus() != 0) {
				throw new RuntimeException("Could not ping Solr cluster at startup");
			}
		} catch (SolrException | IOException | SolrServerException e) {
			throw new RuntimeException("Failed to connect to Solr cluster at startup", e);
		}

		log.info("Successfully pinged Solr cluster at startup");
	}
}
