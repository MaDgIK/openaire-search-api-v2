package eu.openaire.api.config.health;

import eu.openaire.api.repositories.SolrRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class HealthCheck implements HealthIndicator {

	@Autowired
	private SolrRepository solrRepository;

	private final Logger log = LogManager.getLogger(this.getClass());


	@Override
	public Health health() {

		log.info("Health check triggered");

		// Custom health check logic
		boolean isHealthy = checkHealth();
		if (isHealthy) {
			String healthMsg = "Successfully pinged Solr cluster";
			log.info(healthMsg);
			return Health.up().withDetail("message", healthMsg).build();
		} else {
			String notHealthyMsg = "Could not ping Solr cluster; please check the logs for details";
			log.error(notHealthyMsg);
			return Health.down().withDetail("message", notHealthyMsg).build();
		}
	}

	private boolean checkHealth() {

		// Ping Solr cluster to check health
		try {
			if (solrRepository.ping().getStatus() != 0) {
				return false;
			}
		} catch (SolrException | IOException | SolrServerException e) {
			return false;
		}

		return true;
	}
}
