package eu.openaire.api.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "solr")
@Data
public class SolrProperties {

    private String collection;

    private String[] zkHosts;
}
