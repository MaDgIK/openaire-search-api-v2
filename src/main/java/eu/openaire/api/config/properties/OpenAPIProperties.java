package eu.openaire.api.config.properties;

import lombok.Data;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "openapi")
@Data
public class OpenAPIProperties {

    private String serverBaseUrl;

    private String termsUrl;

    private String licenseUrl;

    private String contactUrl;

    private String description;

    @Bean
    public GroupedOpenApi publicApiV1() {
        return GroupedOpenApi.builder()
                .group("OpenAIRE Graph API V1")
                .pathsToMatch("/v1/**")
                .build();
    }

}
