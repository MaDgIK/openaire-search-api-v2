package eu.openaire.api.config;

import eu.openaire.api.config.properties.OpenAPIProperties;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.properties.SwaggerUiConfigParameters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@RequiredArgsConstructor
public class OpenAPIConfig {

    private final OpenAPIProperties openAPIProperties;

    private final SwaggerUiConfigParameters configParameters;


    @PostConstruct
    public void configureSwaggerUi() {
        configParameters.setDefaultModelsExpandDepth(10);
    }

    @Bean
    public OpenAPI configureOpenAPI() {
        Server prodServer = new Server()
                .url(openAPIProperties.getServerBaseUrl())
                .description("Server base URL");

        Contact contact = new Contact()
                .name("Contact us")
                .url(openAPIProperties.getContactUrl());
//        contact.setEmail("bezkoder@gmail.com");
//        contact.setName("BezKoder");
//        contact.setUrl("https://www.bezkoder.com");

        License mitLicense = new License()
                .name("License")
                .url(openAPIProperties.getLicenseUrl());

        Info info = new Info()
                .title("OpenAIRE Graph API")
//                .version("2.0")
                .contact(contact)
                .description(openAPIProperties.getDescription())
                .termsOfService(openAPIProperties.getTermsUrl())
                .license(mitLicense);

        return new OpenAPI()
                .info(info).
                servers(List.of(prodServer));
    }
}
