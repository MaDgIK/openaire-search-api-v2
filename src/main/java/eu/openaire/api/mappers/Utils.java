package eu.openaire.api.mappers;

import eu.openaire.api.errors.exceptions.BadRequestException;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.util.ClientUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Utils {

	private Utils() { }

	public static final String API_PAGE_DESC = """
                    Page number of the results,\s
                    used for basic start/rows pagination.\s
                    Max dataset to retrieve - 10000 records.\s
                    To get more than that, use cursor-based pagination.""";

	public static final String API_PAGE_SIZE_DESC = "Number of results per page";

	/* todo: maybe mention that if a big dataset is required, then download directly the compressed data file
					like this, we avoid high load on this microservice */
	public static final String API_CURSOR_DESC = """
					Cursor-based pagination. Initial value: `cursor=*`.\s
					Cursor should be used when it is required to retrieve a big dataset (more than 10000 records).\s
					To get the next page of results, use nextCursor returned in the response.
					""";

	public static final String API_NEXT_CURSOR_DESC = """
			nextCursor - to be used in the next request to get the next page of results.\s
			You can repeat this process until you’ve fetched as many results as you want,\s
			or until the nextCursor returned matches the current cursor you’ve already specified,\s
			indicating that there are no more results.
			""";

	public static String escapeAndJoin(String[] tokens, String predicate, boolean addQuotes, String suffix) {

		tokens = Arrays.stream(tokens)
				// remove empty tokens
				.map(String::trim)
				.filter(s -> !s.isEmpty())
				// escape special characters and optionally add quotes
				.map(s -> handleInput(s, addQuotes, suffix))
				.toArray(String[]::new);

		return String.join(" " + predicate + " ", tokens);
	}

	public static String handleInput(String token, boolean addQuotes, String suffix) {

		boolean hasLogicalOperator = containsLogicalOperator(token);

		StringBuilder sb = new StringBuilder();

		// Add staring quote, if the token does not start with a quote
		if (addQuotes && !token.startsWith("\"") && !hasLogicalOperator) {
			sb.append("\"");
		}

		// Escape special characters in the token
		sb.append(escapeInput(token, suffix));

		// Add ending quote, if the token does not end with a quote
		if (addQuotes && !token.endsWith("\"") && !hasLogicalOperator) {
			sb.append("\"");
		}

		return sb.toString();
	}

	private static boolean containsLogicalOperator(String input) {
		return input.contains("AND") || input.contains("OR") || input.contains("NOT");
	}

	public static String escapeInput(String input, String suffix) {

		// Split the input into tokens at whitespace or parentheses or quotes
		String[] tokens = input.split("(\\s+|(?=[()\\\"]|(?<=[()\\\"])))");

		// remove empty tokens
		tokens = Arrays.stream(tokens)
				.map(String::trim)
				.filter(s -> !s.isEmpty())
				.toArray(String[]::new);

		int openParentheses = 0;
		int openQuotes = 0;

		List<String> result = new ArrayList<>();

		for (int i = 0; i < tokens.length; i++) {

			// Check if the token is an uppercase boolean operator, a parenthesis, or a quote
			if (tokens[i].equals("AND") || tokens[i].equals("OR") || tokens[i].equals("NOT")) {
				// Leave the token as it is
				result.add(tokens[i]);
				result.add(" ");
			} else if (tokens[i].equals("(")) {
				openParentheses++;
				result.add(tokens[i]);
			} else if (tokens[i].equals(")")) {
				if (openParentheses == 0) {
					throw new BadRequestException("Mismatched parentheses in input: " + input);
				}
				openParentheses--;

				// if the previous character of a closing parenthesis is a space, remove it
				removeLastElementIfSpace(result);

				result.add(tokens[i]);
				result.add(" ");
			} else if (tokens[i].equals("\"")) {
				openQuotes++;

				// Add a space before the token if it is an opening quote
				if (openQuotes % 2 != 0) {
					result.add(tokens[i]);

				// Add a space after the token if it is a closing quote
				} else {
					// if the previous character is a closing quote is a space, remove it
					removeLastElementIfSpace(result);
					result.add(tokens[i]);
					result.add(" ");
				}
			} else {
				// Escape special characters in the other tokens
				tokens[i] = ClientUtils.escapeQueryChars(tokens[i]);

				// append a suffix to the token, e.g., used to append '||orcid' to ORCID ids
				if (!Utils.isNullOrEmpty(suffix)) {
					tokens[i] += suffix;
				}

				result.add(tokens[i]);

				result.add(" ");
			}
		}

		removeLastElementIfSpace(result);

		if (openParentheses != 0) {
			throw new BadRequestException("Mismatched parentheses in input: " + input);
		}

		if (openQuotes % 2 != 0) {
			throw new BadRequestException("Mismatched quotes in input: " + input);
		}


		String value = String.join("", result);

		// if the value starts with the NOT operator, add a wildcard at the beginning
		// NOTE: this is a workaround for Solr, which does not support solely NOT queries
		if (value.startsWith("NOT")) {
			value = "* " + value;
		}

		return value;
	}

	private static void removeLastElementIfSpace(List<String> list) {
		if (list.get(list.size() - 1).equals(" ")) {
			list.remove(list.size() - 1);
		}
	}

	public static boolean isNullOrEmpty(String str) {
		return str == null || str.isBlank();
	}

	public static boolean isNullOrEmpty(String[] str) {
		return str == null || str.length == 0;
	}

	public static String formatSolrDateRange(String fieldName, String fromDate, String toDate) {

		if (fromDate != null && toDate != null) {
			return String.format(fieldName,
					appendHHMMSS(appendMMDD(fromDate, "-01-01")),
							appendHHMMSS(appendMMDD(toDate, "-12-31")));
		} else {
			if (fromDate != null) {
				return String.format(fieldName,
						appendHHMMSS(appendMMDD(fromDate, "-01-01")), "*");
			}

			if (toDate != null) {
				return String.format(fieldName, "*",
						appendHHMMSS(appendMMDD(toDate, "-12-31")));
			}
		}

		return null;
	}

	/***
	 * Append -mm-dd if date came in yyyy format
	 * @return LocalDate in yyyy-mm-dd format
	 */
	private static LocalDate appendMMDD(String date, String monthDaySuffix) {
		try {
			if (date.length() == 10) { //yyyy-mm-dd
				return LocalDate.parse(date);
			} else if (date.length() == 4) { //yyyy
				return LocalDate.parse(date + monthDaySuffix);
			} else {
				throw new IllegalArgumentException("Invalid date format");
			}
		} catch (DateTimeParseException e) {
			throw new IllegalArgumentException("Failed to parse date: " + date, e);
		}
	}

	/***
	 * The incoming date comes in yyyy-mm-dd, so we have to append hh-mm-ss, also.
	 * @return String in yyyy-MM-dd'T'HH:mm:ss'Z' format
	 */
	private static String appendHHMMSS(LocalDate date) {

		// IMPORTANT: all dates are indexed in 12:00:00 in the index (not sure why)
		// so we need to set this time to the date (this should change if dates are indexed in a different way)
		int hour = 12;
		int minute = 0;
		int second = 0;

		LocalDateTime localDateTime = date.atTime(hour, minute, second);

		// Convert LocalDateTime to ZonedDateTime in UTC
		ZonedDateTime zdt = localDateTime.atZone(ZoneOffset.UTC);

		// Define the desired format
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

		// Format the ZonedDateTime to the desired string
		return zdt.format(formatter);
	}

	public static List<SolrQuery.SortClause> formatSortByParam(String sortBy, Map<String, String> fieldMapping) {

		if (Utils.isNullOrEmpty(sortBy)) {
			return null;
		}

		List<SolrQuery.SortClause> sortClauses = new ArrayList<>();

		String[] sortByPairs = sortBy.split(",");
		for (String pair : sortByPairs) {
			String[] parts = pair.trim().split("\\s+");
			String field = parts[0];
			SolrQuery.ORDER order = parts[1].equalsIgnoreCase("ASC") ? SolrQuery.ORDER.asc : SolrQuery.ORDER.desc;

			String fieldName = Optional.ofNullable(fieldMapping.get(field))
					.orElseThrow(() -> new BadRequestException("Invalid field name in sortBy parameter: " + field));

			sortClauses.add(new SolrQuery.SortClause(fieldName, order));
		}

		return sortClauses;
	}
}
