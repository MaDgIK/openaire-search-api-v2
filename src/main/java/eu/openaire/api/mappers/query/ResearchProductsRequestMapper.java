package eu.openaire.api.mappers.query;

import eu.openaire.api.dto.request.v1.ResearchProductsRequest;
import eu.openaire.api.mappers.Utils;
import eu.openaire.api.solr.SolrQueryParams;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

@Mapper(componentModel = "spring")
public interface ResearchProductsRequestMapper {

	@Mapping(target = "start", expression = "java( calculateStart(src.getPage(), src.getPageSize()) )")
	@Mapping(target = "rows", source = "pageSize")
	@Mapping(target = "sort", expression = "java( eu.openaire.api.mappers.Utils.formatSortByParam(src.getSortBy(), SolrFieldsMapper.RESEARCH_PRODUCT_SORT_MAPPING) )")
	SolrQueryParams toSolrQuery(ResearchProductsRequest src);

	@Named("calculateStart")
	default int calculateStart(int page, int pageSize) {
		return (page - 1) * pageSize;
	}

	@AfterMapping
	default void paramsCustomMapping(ResearchProductsRequest src, @MappingTarget SolrQueryParams solrQueryParams) {

		final Map<String, String> solrFieldMapping = SolrFieldsMapper.RESEARCH_PRODUCT_FIELD_MAPPING;

		var qList = new ArrayList<String>();

		// set Q
		if (!Utils.isNullOrEmpty(src.getSearch())) {
			qList.add(String.format(solrFieldMapping.get("search"), Utils.escapeInput(src.getSearch(), null)));
		}

		if (!Utils.isNullOrEmpty(src.getMainTitle())) {
			qList.add(String.format(solrFieldMapping.get("mainTitle"), Utils.escapeInput(src.getMainTitle(), null)));
		}

		if (!Utils.isNullOrEmpty(src.getDescription())) {
			qList.add(String.format(solrFieldMapping.get("description"), Utils.escapeInput(src.getDescription(), null)));
		}

		if (!Utils.isNullOrEmpty(src.getAuthorFullName())) {
			qList.add(String.format(solrFieldMapping.get("authorFullName"), Utils.escapeAndJoin(src.getAuthorFullName(), "OR", false, null)));
		}

		if (src.getHasProjectRel() != null) {
			String hasProjectField = solrFieldMapping.get("hasProjectRel");

			// if param was given and is false, negate the field
			if (!src.getHasProjectRel()) {
				hasProjectField = "-" + hasProjectField;
			}

			qList.add(hasProjectField);
		}

		// form query string
		if (!qList.isEmpty()) {
			solrQueryParams.setQueryString(String.join(" AND ", qList));
		}

		// set FQ
		var fqList = new ArrayList<String>();

		fqList.add(String.format("oaftype:\"%s\"", "result"));
		fqList.add(String.format("deletedbyinference:%s", "false"));
		fqList.add(String.format("-status:\"%s\"", "under curation"));

		if (!Utils.isNullOrEmpty(src.getId())) {
			fqList.add(String.format(solrFieldMapping.get("id"), Utils.escapeAndJoin(src.getId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getOriginalId())) {
			fqList.add(String.format(solrFieldMapping.get("originalId"), Utils.escapeAndJoin(src.getOriginalId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getPid())) {
			fqList.add(String.format(solrFieldMapping.get("pid"), Utils.escapeAndJoin(src.getPid(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getType())) {
			fqList.add(String.format(solrFieldMapping.get("type"), Utils.escapeAndJoin(src.getType(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getCountryCode())) {
			fqList.add(String.format(solrFieldMapping.get("countryCode"), Utils.escapeAndJoin(src.getCountryCode(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getAuthorOrcid())) {

			// append suffix "||orcid"
//			String[] authorOrcidWithSuffix = Arrays.stream(src.getAuthorOrcid())
//					.map(s -> s + "||orcid")
//					.toArray(String[]::new);
			fqList.add(String.format(solrFieldMapping.get("authorOrcid"), Utils.escapeAndJoin(src.getAuthorOrcid(), "OR", false, "||orcid")));
		}

		if (!Utils.isNullOrEmpty(src.getBestOpenAccessRightLabel())) {

			// Define the mapping from the original values to the new values
			Map<String, String> bestAccessRightLabelMapping = Map.of(
					"OPEN SOURCE", "Open Source",
					"OPEN", "Open Access",
					"EMBARGO", "Embargo",
					"RESTRICTED", "Restricted",
					"CLOSED", "Closed Access",
					"UNKNOWN", "not available"
			);

			// Replace the bestOpenAccessRightLabel values
			String[] mappedBestAccessRightLabels = Arrays.stream(src.getBestOpenAccessRightLabel())
					.map(s -> bestAccessRightLabelMapping.getOrDefault(s, s))
					.toArray(String[]::new);

			fqList.add(String.format(solrFieldMapping.get("bestAccessRightLabel"), Utils.escapeAndJoin(mappedBestAccessRightLabels, "OR", true, null)));
		}

		if (!Utils.isNullOrEmpty(src.getSubjects())) {
			fqList.add(String.format(solrFieldMapping.get("subjects"), Utils.escapeAndJoin(src.getSubjects(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getPublisher())) {
			fqList.add(String.format(solrFieldMapping.get("publisher"), Utils.escapeAndJoin(src.getPublisher(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getInfluenceClass())) {
			fqList.add(String.format(solrFieldMapping.get("influence"), Utils.escapeAndJoin(src.getInfluenceClass(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getPopularityClass())) {
			fqList.add(String.format(solrFieldMapping.get("popularity"), Utils.escapeAndJoin(src.getPopularityClass(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getImpulseClass())) {
			fqList.add(String.format(solrFieldMapping.get("impulse"), Utils.escapeAndJoin(src.getImpulseClass(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getCitationCountClass())) {
			fqList.add(String.format(solrFieldMapping.get("citationCount"), Utils.escapeAndJoin(src.getCitationCountClass(), "OR", false, null)));
		}


		// related entity fields
		if (!Utils.isNullOrEmpty(src.getRelCommunityId())) {
			fqList.add(String.format(solrFieldMapping.get("relCommunityId"), Utils.escapeAndJoin(src.getRelCommunityId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getRelOrganizationId())) {
			fqList.add(String.format(solrFieldMapping.get("relOrganizationId"), Utils.escapeAndJoin(src.getRelOrganizationId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getRelProjectId())) {
			fqList.add(String.format(solrFieldMapping.get("relProjectId"), Utils.escapeAndJoin(src.getRelProjectId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getRelProjectCode())) {
			fqList.add(String.format(solrFieldMapping.get("relProjectCode"), Utils.escapeAndJoin(src.getRelProjectCode(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getRelProjectFundingShortName())) {
			fqList.add(String.format(solrFieldMapping.get("relProjectFundingShortName"), Utils.escapeAndJoin(src.getRelProjectFundingShortName(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getRelProjectFundingStreamId())) {
			String relProjectFundingStreamId = Utils.escapeAndJoin(src.getRelProjectFundingStreamId(), "OR", true, null);
			fqList.add(String.format(solrFieldMapping.get("relProjectFundingStreamId"), relProjectFundingStreamId, relProjectFundingStreamId, relProjectFundingStreamId));
		}

		if (!Utils.isNullOrEmpty(src.getRelHostingDataSourceId())) {
			fqList.add(String.format(solrFieldMapping.get("relHostingDataSourceId"), Utils.escapeAndJoin(src.getRelHostingDataSourceId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getRelCollectedFromDatasourceId())) {
			fqList.add(String.format(solrFieldMapping.get("relCollectedFromDatasourceId"), Utils.escapeAndJoin(src.getRelCollectedFromDatasourceId(), "OR", false, null)));
		}

		// Only for publications
		if (!Utils.isNullOrEmpty(src.getInstanceType())) {
			fqList.add(String.format(solrFieldMapping.get("instanceType"), Utils.escapeAndJoin(src.getInstanceType(), "OR", true, null)));
		}

		if (src.getSdg() != null) {
			String[] sdgValues = Arrays.stream(src.getSdg())
					.map(sdg -> sdg + ".*")  // Append ".*" to match the sdg numeric prefix
					.toArray(String[]::new);

			fqList.add(String.format(solrFieldMapping.get("sdg"), String.join(" OR ", sdgValues)));
		}

		if (!Utils.isNullOrEmpty(src.getFos())) {
			fqList.add(String.format(solrFieldMapping.get("fos"), Utils.escapeAndJoin(src.getFos(), "OR", true, null)));
		}

		if (src.getIsPeerReviewed() != null) {
			fqList.add(String.format(solrFieldMapping.get("isPeerReviewed"), src.getIsPeerReviewed()));
		}

		if (src.getIsInDiamondJournal() != null) {
			fqList.add(String.format(solrFieldMapping.get("isInDiamondJournal"), src.getIsInDiamondJournal()));
		}

		if (src.getIsPubliclyFunded() != null) {
			fqList.add(String.format(solrFieldMapping.get("isPubliclyFunded"), src.getIsPubliclyFunded()));
		}

		if (src.getIsGreen() != null) {
			fqList.add(String.format(solrFieldMapping.get("isGreen"), src.getIsGreen()));
		}

		if (!Utils.isNullOrEmpty(src.getOpenAccessColor())) {
			fqList.add(String.format(solrFieldMapping.get("openAccessColor"), Utils.escapeAndJoin(src.getOpenAccessColor(), "OR", false, null)));
		}

		// publication date
		Optional.ofNullable(
				Utils.formatSolrDateRange(
						solrFieldMapping.get("publicationDate"),
						src.getFromPublicationDate(),
						src.getToPublicationDate()
				)
		).ifPresent(fqList::add);

		solrQueryParams.setFilterQueries(fqList);
	}

}
