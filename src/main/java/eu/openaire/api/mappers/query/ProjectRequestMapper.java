package eu.openaire.api.mappers.query;

import eu.openaire.api.dto.request.v1.ProjectRequest;
import eu.openaire.api.mappers.Utils;
import eu.openaire.api.solr.SolrQueryParams;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

@Mapper(componentModel = "spring")
public interface ProjectRequestMapper {

	@Mapping(target = "start", expression = "java( calculateStart(src.getPage(), src.getPageSize()) )")
	@Mapping(target = "rows", source = "pageSize")
	@Mapping(target = "sort", expression = "java( eu.openaire.api.mappers.Utils.formatSortByParam(src.getSortBy(), SolrFieldsMapper.PROJECT_SORT_MAPPING) )")
	SolrQueryParams toSolrQuery(ProjectRequest src);

	@Named("calculateStart")
	default int calculateStart(int page, int pageSize) {
		return (page - 1) * pageSize;
	}

	@AfterMapping
	default void paramsCustomMapping(ProjectRequest src, @MappingTarget SolrQueryParams solrQueryParams) {

		final Map<String, String> solrFieldMapping = SolrFieldsMapper.PROJECT_FIELD_MAPPING;

		var qList = new ArrayList<String>();

		// set Q
		if (!Utils.isNullOrEmpty(src.getSearch())) {
			qList.add(String.format(solrFieldMapping.get("search"), Utils.escapeInput(src.getSearch(), null)));
		}

		if (!Utils.isNullOrEmpty(src.getTitle())) {
			qList.add(String.format(solrFieldMapping.get("title"), Utils.escapeInput(src.getTitle(), null)));
		}

		if (!Utils.isNullOrEmpty(src.getKeywords())) {
			qList.add(String.format(solrFieldMapping.get("keywords"), Utils.escapeInput(src.getKeywords(), null)));
		}

		// form query string
		if (!qList.isEmpty()) {
			solrQueryParams.setQueryString(String.join(" AND ", qList));
		}

		// set FQ
		var fqList = new ArrayList<String>();

		fqList.add(String.format("oaftype:\"%s\"", "project"));
		fqList.add(String.format("deletedbyinference:%s", "false"));
		fqList.add(String.format("-status:\"%s\"", "under curation"));

		if (!Utils.isNullOrEmpty(src.getId())) {
			fqList.add(String.format(solrFieldMapping.get("id"), Utils.escapeAndJoin(src.getId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getCode())) {
			fqList.add(String.format(solrFieldMapping.get("code"), Utils.escapeAndJoin(src.getCode(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getAcronym())) {
			fqList.add(String.format(solrFieldMapping.get("acronym"), Utils.escapeAndJoin(src.getAcronym(), "OR", true, null)));
		}

		if (!Utils.isNullOrEmpty(src.getFundingShortName())) {
			fqList.add(String.format(solrFieldMapping.get("fundingShortName"), Utils.escapeAndJoin(src.getFundingShortName(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getFundingStreamId())) {
			String fundingStreamId = Utils.escapeAndJoin(src.getFundingStreamId(), "OR", true, null);
			fqList.add(String.format(solrFieldMapping.get("fundingStreamId"), fundingStreamId, fundingStreamId, fundingStreamId));
		}

		if (!Utils.isNullOrEmpty(src.getCallIdentifier())) {
			fqList.add(String.format(solrFieldMapping.get("callIdentifier"), Utils.escapeAndJoin(src.getCallIdentifier(), "OR", true, null)));
		}

		// related entity fields
		if (!Utils.isNullOrEmpty(src.getRelCommunityId())) {
			fqList.add(String.format(solrFieldMapping.get("relCommunityId"), Utils.escapeAndJoin(src.getRelCommunityId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getRelOrganizationCountryCode())) {
			fqList.add(String.format(solrFieldMapping.get("relOrganizationCountryCode"), Utils.escapeAndJoin(src.getRelOrganizationCountryCode(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getRelOrganizationName())) {
			String relOrganizationName = Utils.escapeAndJoin(src.getRelOrganizationName(), "OR", false, null);
			fqList.add(String.format(solrFieldMapping.get("relOrganizationName"), relOrganizationName, relOrganizationName));
		}

		if (!Utils.isNullOrEmpty(src.getRelOrganizationId())) {
			fqList.add(String.format(solrFieldMapping.get("relOrganizationId"), Utils.escapeAndJoin(src.getRelOrganizationId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getRelCollectedFromDatasourceId())) {
			fqList.add(String.format(solrFieldMapping.get("relCollectedFromDatasourceId"), Utils.escapeAndJoin(src.getRelCollectedFromDatasourceId(), "OR", false, null)));
		}

		// add start year range filter
		Optional.ofNullable(
				Utils.formatSolrDateRange(
						solrFieldMapping.get("startDate"),
						src.getFromStartDate(),
						src.getToStartDate()
				)
		).ifPresent(fqList::add);

		// add end year range filter
		Optional.ofNullable(
				Utils.formatSolrDateRange(
						solrFieldMapping.get("endDate"),
						src.getFromEndDate(),
						src.getToEndDate()
				)
		).ifPresent(fqList::add);

		solrQueryParams.setFilterQueries(fqList);

	}

}
