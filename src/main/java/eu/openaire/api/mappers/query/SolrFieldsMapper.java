package eu.openaire.api.mappers.query;

import java.util.Map;

public class SolrFieldsMapper {

	// mappings for the organization entity
	public static final Map<String, String> ORGANIZATION_FIELDS_MAPPING = java.util.Map.ofEntries(

			// search field mapping
			Map.entry("search", "__all:(%s)"),
			Map.entry("legalName", "organizationlegalname:(%s)"),
			Map.entry("legalShortName", "organizationlegalshortname:(%s)"),

			// filter query field mapping
			Map.entry("id", "__indexrecordidentifier:(%s)"),
			Map.entry("pid", "pid:(%s)"),
			Map.entry("countryCode", "countrynojurisdiction:(%s)"),

			// filter by related entity fields
			Map.entry("relCommunityId", "communityid:(%s)"),
			Map.entry("relCollectedFromDatasourceId", "collectedfromdatasourceid:(%s)")

	);

	public static final Map<String, String> ORGANIZATION_SORT_MAPPING = Map.ofEntries(
			Map.entry("relevance", "score")
	);

	// mappings for the datasource entity
	public static final Map<String, String> DATASOURCE_FIELD_MAPPING = Map.ofEntries(

			// search field mapping
			Map.entry("search", "__all:(%s)"),
			Map.entry("officialName", "datasourceofficialname:(%s)"),
			Map.entry("englishName", "datasourceenglishname:(%s)"),

			// filter query field mapping
			Map.entry("id", "__indexrecordidentifier:(%s)"),
			Map.entry("pid", "pid:(%s)"),
			Map.entry("subjects", "datasourceodsubjects:(%s)"),
			Map.entry("dataSourceTypeName", "datasourcetypename:(%s)"),
			Map.entry("contentTypes", "datasourceodcontenttypes:(%s)"),

			// filter by related entity fields
			Map.entry("relOrganizationId", "relorganizationid:(%s)"),
			Map.entry("relCommunityId", "communityid:(%s)"),
			Map.entry("relCollectedFromDatasourceId", "collectedfromdatasourceid:(%s)")
	);

	public static final Map<String, String> DATASOURCE_SORT_MAPPING = Map.ofEntries(
			Map.entry("relevance", "score")
	);


	// mappings for the project entity
	public static final Map<String, String> PROJECT_FIELD_MAPPING = Map.ofEntries(

			// search field mapping
			Map.entry("search", "__all:(%s)"),
			Map.entry("title", "projecttitle:(%s)"),
			Map.entry("keywords", "projectkeywords:(%s)"),

			// filter query field mapping
			Map.entry("id", "__indexrecordidentifier:(%s)"),
			Map.entry("code", "projectcode_nt:(%s)"),
			Map.entry("acronym", "projectacronym:(%s)"),
			Map.entry("fundingShortName", "fundershortname:(%s)"),
			Map.entry("fundingStreamId", "fundinglevel0_name:(%s) OR fundinglevel1_name:(%s) OR fundinglevel2_name:(%s)"),
			Map.entry("callIdentifier", "projectcallidentifier:(%s)"),

			Map.entry("startDate", "projectstartdate:[%s TO %s]"),
			Map.entry("endDate", "projectenddate:[%s TO %s]"),

			// filter by related entity fields
			Map.entry("relCommunityId", "communityid:(%s)"),
			Map.entry("relOrganizationCountryCode", "relorganizationcountryid:(%s)"),
			Map.entry("relOrganizationName", "relorganizationname: (%s) OR relorganizationshortname: (%s)"),
			Map.entry("relOrganizationId", "relorganizationid:(%s)"),
			Map.entry("relCollectedFromDatasourceId", "collectedfromdatasourceid:(%s)")

	);

	public static final Map<String, String> PROJECT_SORT_MAPPING = Map.ofEntries(
			Map.entry("relevance", "score"),
			Map.entry("startDate", "projectstartyear"),
			Map.entry("endDate", "projectendyear")
	);

	public static final Map<String, String> RESEARCH_PRODUCT_FIELD_MAPPING = Map.ofEntries(

			// search field mapping
			Map.entry("search", "__all:(%s)"),
			Map.entry("mainTitle", "resulttitle:(%s)"),
			Map.entry("description", "resultdescription:(%s)"),
			Map.entry("authorFullName", "resultauthor:(%s)"),

			// filter query field mapping
			Map.entry("id", "__indexrecordidentifier:(%s)"),
			Map.entry("originalId", "originalid:(%s)"),
			Map.entry("pid", "pid:(%s)"),
			Map.entry("type", "resulttypeid:(%s)"),
			Map.entry("countryCode", "country:(%s)"),
			Map.entry("authorOrcid", "orcidtypevalue:(%s)"),
			Map.entry("bestAccessRightLabel", "resultbestaccessright:(%s)"),
			Map.entry("subjects", "resultsubject:(%s)"),
			Map.entry("publisher", "resultpublisher:(%s)"),
			Map.entry("publicationDate", "resultdateofacceptance:[%s TO %s]"),
			Map.entry("influence", "influence_class:(%s)"),
			Map.entry("popularity", "popularity_class:(%s)"),
			Map.entry("impulse", "impulse_class:(%s)"),
			Map.entry("citationCount", "citation_count_class:(%s)"),

			// related entity fields
			Map.entry("relOrganizationId", "relorganizationid:(%s)"),
			Map.entry("relCommunityId", "communityid:(%s)"),
			Map.entry("relProjectId", "relprojectid:(%s)"),
			Map.entry("relProjectCode", "relprojectcode:(%s)"),
			Map.entry("hasProjectRel", "relprojectid:[* TO *]"),
			Map.entry("relProjectFundingShortName", "relfundershortname:(%s)"),
			Map.entry("relProjectFundingStreamId", "relfundinglevel0_name:(%s) OR relfundinglevel1_name:(%s) OR relfundinglevel2_name:(%s)"),
			Map.entry("relHostingDataSourceId", "resulthostingdatasourceid:(%s)"),
			Map.entry("relCollectedFromDatasourceId", "collectedfromdatasourceid:(%s)"),

			// only for publications
			Map.entry("instanceType", "instancetypename:(%s)"),
			Map.entry("sdg", "sdg:(%s)"),
			Map.entry("fos", "fos:(%s)"),
			Map.entry("isPeerReviewed", "peerreviewed:(%s)"),
			Map.entry("isInDiamondJournal", "isindiamondjournal:(%s)"),
			Map.entry("isPubliclyFunded", "publiclyfunded:(%s)"),
			Map.entry("isGreen", "isgreen:(%s)"),
			Map.entry("openAccessColor", "openaccesscolor:(%s)")

	);

	public static final Map<String, String> RESEARCH_PRODUCT_SORT_MAPPING = Map.ofEntries(

			Map.entry("relevance", "score"),
			Map.entry("publicationDate", "resultdateofacceptance"),
			Map.entry("dateOfCollection", "dateofcollection"),

			Map.entry("influence", "influence"),
			Map.entry("popularity", "popularity"),
			Map.entry("citationCount", "citation_count"),
			Map.entry("impulse", "impulse")
	);


}
