package eu.openaire.api.mappers.query;

import eu.openaire.api.dto.request.v1.OrganizationRequest;
import eu.openaire.api.mappers.Utils;
import eu.openaire.api.solr.SolrQueryParams;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

import java.util.ArrayList;
import java.util.Map;

@Mapper(componentModel = "spring")
public interface OrganizationRequestMapper {

	@Mapping(target = "start", expression = "java( calculateStart(src.getPage(), src.getPageSize()) )")
	@Mapping(target = "rows", source = "pageSize")
	@Mapping(target = "sort", expression = "java( eu.openaire.api.mappers.Utils.formatSortByParam(src.getSortBy(), SolrFieldsMapper.ORGANIZATION_SORT_MAPPING) )")
	SolrQueryParams toSolrQuery(OrganizationRequest src);

	@Named("calculateStart")
	default int calculateStart(int page, int pageSize) {
		return (page - 1) * pageSize;
	}

	@AfterMapping
	default void paramsCustomMapping(OrganizationRequest src, @MappingTarget SolrQueryParams solrQueryParams) {

		final Map<String, String> solrFieldMapping = SolrFieldsMapper.ORGANIZATION_FIELDS_MAPPING;

		var qList = new ArrayList<String>();

		// set Q
		if (!Utils.isNullOrEmpty(src.getSearch())) {
			qList.add(String.format(solrFieldMapping.get("search"), Utils.escapeInput(src.getSearch(), null)));
		}

		if (!Utils.isNullOrEmpty(src.getLegalName())) {
			qList.add(String.format(solrFieldMapping.get("legalName"), Utils.escapeAndJoin(src.getLegalName(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getLegalShortName())) {
			qList.add(String.format(solrFieldMapping.get("legalShortName"), Utils.escapeAndJoin(src.getLegalShortName(), "OR", false, null)));
		}

		// form query string
		if (!qList.isEmpty()) {
			solrQueryParams.setQueryString(String.join(" AND ", qList));
		}

		// set FQ
		var fqList = new ArrayList<String>();

		fqList.add(String.format("oaftype:\"%s\"", "organization"));
		fqList.add(String.format("deletedbyinference:%s", "false"));
		fqList.add(String.format("reldatasourcecompatibilityid:(%s) OR relproject:[* TO *]", "\"driver\" OR \"driver-openaire2.0\" OR \"openaire2.0\" OR \"openaire3.0\" OR \"openaire4.0\" OR \"openaire-cris_1.1\" OR \"openaire2.0_data\" OR \"hostedBy\""));
		fqList.add(String.format("-status:\"%s\"", "under curation"));

		if (!Utils.isNullOrEmpty(src.getId())) {
			fqList.add(String.format(solrFieldMapping.get("id"), Utils.escapeAndJoin(src.getId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getPid())) {
			fqList.add(String.format(solrFieldMapping.get("pid"), Utils.escapeAndJoin(src.getPid(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getCountryCode())) {
			fqList.add(String.format(solrFieldMapping.get("countryCode"), Utils.escapeAndJoin(src.getCountryCode(), "OR", false, null)));
		}

		// related entity fields
		if (!Utils.isNullOrEmpty(src.getRelCommunityId())) {
			fqList.add(String.format(solrFieldMapping.get("relCommunityId"), Utils.escapeAndJoin(src.getRelCommunityId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getRelCollectedFromDatasourceId())) {
			fqList.add(String.format(solrFieldMapping.get("relCollectedFromDatasourceId"), Utils.escapeAndJoin(src.getRelCollectedFromDatasourceId(), "OR", false, null)));
		}

		solrQueryParams.setFilterQueries(fqList);
	}

}
