package eu.openaire.api.mappers.query;

import eu.openaire.api.dto.request.v1.DataSourceRequest;
import eu.openaire.api.mappers.Utils;
import eu.openaire.api.solr.SolrQueryParams;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

import java.util.ArrayList;
import java.util.Map;

@Mapper(componentModel = "spring")
public interface DataSourceRequestMapper {

	@Mapping(target = "start", expression = "java( calculateStart(src.getPage(), src.getPageSize()) )")
	@Mapping(target = "rows", source = "pageSize")
	@Mapping(target = "cursor", source = "cursor")
	@Mapping(target = "sort", expression = "java( eu.openaire.api.mappers.Utils.formatSortByParam(src.getSortBy(), SolrFieldsMapper.DATASOURCE_SORT_MAPPING) )")
	SolrQueryParams toSolrQuery(DataSourceRequest src);

	@Named("calculateStart")
	default int calculateStart(int page, int pageSize) {
		return (page - 1) * pageSize;
	}

	@AfterMapping
	default void paramsCustomMapping(DataSourceRequest src, @MappingTarget SolrQueryParams solrQueryParams) {

		final Map<String, String> solrFieldMapping = SolrFieldsMapper.DATASOURCE_FIELD_MAPPING;

		var qList = new ArrayList<String>();

		// set Q
		if (!Utils.isNullOrEmpty(src.getSearch())) {
			qList.add(String.format(solrFieldMapping.get("search"), Utils.escapeInput(src.getSearch(), null)));
		}

		if (!Utils.isNullOrEmpty(src.getOfficialName())) {
			qList.add(String.format(solrFieldMapping.get("officialName"), Utils.escapeAndJoin(src.getOfficialName(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getEnglishName())) {
			qList.add(String.format(solrFieldMapping.get("englishName"), Utils.escapeAndJoin(src.getEnglishName(), "OR", false, null)));
		}

		// form query string
		if (!qList.isEmpty()) {
			solrQueryParams.setQueryString(String.join(" AND ", qList));
		}

		// set FQ
		var fqList = new ArrayList<String>();

		fqList.add(String.format("oaftype:\"%s\"", "datasource"));
		fqList.add(String.format("eosctype:\"%s\"", "Data Source"));
		fqList.add(String.format("deletedbyinference:%s", "false"));
		fqList.add(String.format("-status:\"%s\"", "under curation"));

		if (!Utils.isNullOrEmpty(src.getId())) {
			fqList.add(String.format(solrFieldMapping.get("id"), Utils.escapeAndJoin(src.getId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getPid())) {
			fqList.add(String.format(solrFieldMapping.get("pid"), Utils.escapeAndJoin(src.getPid(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getSubjects())) {
			fqList.add(String.format(solrFieldMapping.get("subjects"), Utils.escapeAndJoin(src.getSubjects(), "OR", true, null)));
		}

		if (!Utils.isNullOrEmpty(src.getDataSourceTypeName())) {
			fqList.add(String.format(solrFieldMapping.get("dataSourceTypeName"), Utils.escapeAndJoin(src.getDataSourceTypeName(), "OR", true, null)));
		}

		if (!Utils.isNullOrEmpty(src.getContentTypes())) {
			fqList.add(String.format(solrFieldMapping.get("contentTypes"), Utils.escapeAndJoin(src.getContentTypes(), "OR", true, null)));
		}

		// related entity fields
		if (!Utils.isNullOrEmpty(src.getRelOrganizationId())) {
			fqList.add(String.format(solrFieldMapping.get("relOrganizationId"), Utils.escapeAndJoin(src.getRelOrganizationId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getRelCommunityId())) {
			fqList.add(String.format(solrFieldMapping.get("relCommunityId"), Utils.escapeAndJoin(src.getRelCommunityId(), "OR", false, null)));
		}

		if (!Utils.isNullOrEmpty(src.getRelCollectedFromDatasourceId())) {
			fqList.add(String.format(solrFieldMapping.get("relCollectedFromDatasourceId"), Utils.escapeAndJoin(src.getRelCollectedFromDatasourceId(), "OR", false, null)));
		}

		solrQueryParams.setFilterQueries(fqList);
	}

}
