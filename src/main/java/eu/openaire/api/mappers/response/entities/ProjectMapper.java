package eu.openaire.api.mappers.response.entities;

import eu.dnetlib.dhp.oa.model.Indicator;
import eu.dnetlib.dhp.oa.model.graph.Funder;
import eu.dnetlib.dhp.oa.model.graph.Fundings;
import eu.dnetlib.dhp.oa.model.graph.Granted;
import eu.dnetlib.dhp.oa.model.graph.Project;
import eu.dnetlib.dhp.schema.solr.Funding;
import eu.dnetlib.dhp.schema.solr.FundingLevel;
import eu.dnetlib.dhp.schema.solr.Measure;
import eu.dnetlib.dhp.schema.solr.SolrRecord;
import eu.dnetlib.dhp.schema.solr.Subject;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface ProjectMapper {
//no indicator for project are present in the dump. Consider to include them
    @Mapping(target = "id", source = "payload.header.id")
    @Mapping(target = "websiteUrl", source = "payload.project.websiteurl")
    @Mapping(target = "acronym", source = "payload.project.acronym")
    @Mapping(target = "title", source = "payload.project.title")
    @Mapping(target = "startDate", source = "payload.project.startdate")
    @Mapping(target = "endDate", source = "payload.project.enddate")
    @Mapping(target = "callIdentifier", source = "payload.project.callidentifier")
    @Mapping(target = "keywords", source = "payload.project.keywords")
    @Mapping(target = "subjects", expression = "java( mapSubjects(payload.getProject().getSubjects()) )")
    @Mapping(target = "summary", source = "payload.project.summary")
    @Mapping(target = "code", source = "payload.project.code")
    @Mapping(target = "granted", expression = " java(mapFundedAmount(payload.getProject()))")
    @Mapping(target = "openAccessMandateForDataset", expression = "java( mapOpenAccessMandate(payload.getProject().getEcarticle29_3()) )")
    @Mapping(target = "fundings", expression = "java( mapFundings(payload.getProject().getFunding()) )")
    Project toGraphProject(SolrRecord payload);

    @Named("mapFundedAmount")
    @Mapping(target = "currency", source = "currency")
    @Mapping(target = "totalCost", source = "totalcost")
    @Mapping(target = "fundedAmount", source = "fundedamount")
    Granted mapFundedAmount(eu.dnetlib.dhp.schema.solr.Project project);

    @Named("mapSubjects")
    default List<String> mapSubjects(List<Subject>subjectList) {
        if (subjectList == null) {
            return null;
        }
        return subjectList.stream().map(Subject::getValue).distinct().collect(Collectors.toList());
    }

    @Named("mapOpenAccessMandate")
    default Boolean mapOpenAccessMandate(String mandate) {
        if (mandate == null) {
            return Boolean.FALSE;
        }
        return Boolean.parseBoolean(mandate);
    }


    @Named("mapFunding")
    default List<Funder> mapFundings(Funding funding) {
        if (funding == null) {
            return null;
        }
        return Collections.singletonList(mapFunding(funding));
    }

    @Named("mapFunding")
    @Mapping(target = "shortName", source = "funding.funder.shortname")
    @Mapping(target = "name", source = "funding.funder.name")
    @Mapping(target = "jurisdiction", source = "funding.funder.jurisdiction.code")
   // @Mapping(target = "fundingStream", expression = "java ( mapFundingStream(funding))")
    Funder mapFunding(Funding funding);

    @Named("mapFundingStream")
    default Fundings mapFundingStream(Funding funding) {
        Fundings fundings = new Fundings();
        StringBuffer description = new StringBuffer();
        FundingLevel fl = funding.getLevel0();
        String id ;
        description.append(fl.getDescription());
        id = fl.getId();
        if (Optional.ofNullable(funding.getLevel1()).isPresent()) {
            fl = funding.getLevel1();
            id = fl.getId();
            description.append(" - ").append(fl.getDescription());
        }
        if (Optional.ofNullable(funding.getLevel2()).isPresent()) {
            fl = funding.getLevel2();
            id = fl.getId();
            description.append(" - ").append(fl.getDescription());
        }
        fundings.setId(id);
        fundings.setDescription(description.toString());
        return fundings;
    }

    @Named("mapIndicators")
    default Indicator mapIndicators(List<Measure> measureList) {
        return Utils.mapIndicators(measureList);
    }

}
