package eu.openaire.api.mappers.response.entities;

import com.google.common.collect.Maps;
import eu.dnetlib.dhp.schema.common.ModelConstants;

import java.util.Map;

public class Constants {
    public static final String CABF2 = "c_abf2";
    public static final String C16EC = "c_16ec";
    public static final String C14CB = "c_14cb";
    public static final String CF1CF = "c_f1cf";
    public static final String COAR_ACCESS_RIGHT_SCHEMA = "http://vocabularies.coar-repositories.org/documentation/access_rights/";

    public static final String USAGE_COUNT_DOWNLOADS = "downloads";
    public static final String USAGE_COUNT_VIEWS = "views";
    public static final String BIP_POPULARITY = "popularity";
    public static final String BIP_POPULARITY_ALT = "popularity_alt";
    public static final String BIP_INFLUENCE = "influence";
    public static final String BIP_INFLUENCE_ALT = "influence_alt";
    public static final String BIP_IMPULSE = "impulse";

    public static final Map<String, String> ACCESS_RIGHTS_COAR_MAP = Maps.newHashMap();

    static {
        ACCESS_RIGHTS_COAR_MAP.put(ModelConstants.ACCESS_RIGHT_OPEN, CABF2);
        ACCESS_RIGHTS_COAR_MAP.put("RESTRICTED", C16EC);
        ACCESS_RIGHTS_COAR_MAP.put("OPEN SOURCE", CABF2);
        ACCESS_RIGHTS_COAR_MAP.put(ModelConstants.ACCESS_RIGHT_CLOSED, C14CB);
        ACCESS_RIGHTS_COAR_MAP.put(ModelConstants.ACCESS_RIGHT_EMBARGO, CF1CF);
    }

    public static final Map<String, String> COAR_CODE_LABEL_MAP = Maps.newHashMap();
    static {
        COAR_CODE_LABEL_MAP.put(CABF2, ModelConstants.ACCESS_RIGHT_OPEN);
        COAR_CODE_LABEL_MAP.put(C16EC, "RESTRICTED");
        COAR_CODE_LABEL_MAP.put(C14CB, ModelConstants.ACCESS_RIGHT_CLOSED);
        COAR_CODE_LABEL_MAP.put(CF1CF, ModelConstants.ACCESS_RIGHT_EMBARGO);
    }
}
