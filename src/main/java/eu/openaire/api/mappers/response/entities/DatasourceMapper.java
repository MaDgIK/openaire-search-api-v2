package eu.openaire.api.mappers.response.entities;

import eu.dnetlib.dhp.oa.model.Container;
import eu.dnetlib.dhp.oa.model.Indicator;
import eu.dnetlib.dhp.oa.model.graph.Datasource;
import eu.dnetlib.dhp.oa.model.graph.DatasourcePid;
import eu.dnetlib.dhp.oa.model.graph.DatasourceSchemeValue;
import eu.dnetlib.dhp.schema.solr.CodeLabel;
import eu.dnetlib.dhp.schema.solr.Journal;
import eu.dnetlib.dhp.schema.solr.Measure;
import eu.dnetlib.dhp.schema.solr.Pid;
import eu.dnetlib.dhp.schema.solr.SolrRecord;
import eu.dnetlib.dhp.schema.solr.Subject;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface DatasourceMapper {

    @Mapping(target = "pids", expression = "java( mapPids(payload.getPid()) )")
    @Mapping(target = "id", source = "payload.header.id")
    @Mapping(target = "originalIds", source = "payload.header.originalId")
    @Mapping(target = "type", expression = "java( mapDatasourceType(payload.getDatasource().getDatasourcetype()))")
    @Mapping(target = "openaireCompatibility", source = "payload.datasource.openairecompatibility.label")
    @Mapping(target = "officialName", source = "payload.datasource.officialname")
    @Mapping(target = "englishName", source = "payload.datasource.englishname")
    @Mapping(target = "websiteUrl", source = "payload.datasource.websiteurl")
    @Mapping(target = "logoUrl", source = "payload.datasource.logourl")
    @Mapping(target = "dateOfValidation", source = "payload.datasource.dateofvalidation")
    @Mapping(target = "description", source = "payload.datasource.description")
    @Mapping(target = "subjects", expression = "java( mapSubjects(payload.getDatasource().getSubjects()) )")
    @Mapping(target = "languages", source = "payload.datasource.odlanguages")
    @Mapping(target = "contentTypes", source = "payload.datasource.odcontenttypes")
    @Mapping(target = "releaseStartDate", source = "payload.datasource.releasestartdate")
    @Mapping(target = "releaseEndDate", source = "payload.datasource.releaseenddate")
    @Mapping(target = "missionStatementUrl", source = "payload.datasource.missionstatementurl")
    @Mapping(target = "accessRights", source = "payload.datasource.databaseaccesstype")
    @Mapping(target = "uploadRights", source = "payload.datasource.datauploadtype")
    @Mapping(target = "databaseAccessRestriction", source = "payload.datasource.databaseaccessrestriction")
    @Mapping(target = "dataUploadRestriction", source = "payload.datasource.datauploadrestriction")
    @Mapping(target = "versioning", source = "payload.datasource.versioning")
    @Mapping(target = "citationGuidelineUrl", source = "payload.datasource.citationguidelineurl")
    @Mapping(target = "pidSystems", source = "payload.datasource.pidsystems")
    @Mapping(target = "certificates", source = "payload.datasource.certificates")
    @Mapping(target = "policies", expression = "java( mapPolicies(payload.getDatasource().getPolicies()) )")
    @Mapping(target = "journal", expression = "java( mapJournal(payload.getDatasource().getJournal()) )")
    Datasource toGraphDatasource(SolrRecord payload);

    @Named("mapDatasourceType")
    @Mapping(target = "scheme", source = "code")
    @Mapping(target = "value", source = "label")
    DatasourceSchemeValue mapDatasourceType(CodeLabel codeLabel) ;

    @Mapping(target = "scheme", source = "typeCode")
    DatasourcePid mapPid(Pid pid);

    @Named("mapPids")
    default List<DatasourcePid> mapPids(List<Pid> pids ) {
        if (pids == null) {
            return null;
        }
        return pids.stream().map(this::mapPid).collect(Collectors.toList());
    }

    @Named("mapSubjects")
    default List<String> mapSubjects(List<Subject> sbjs ) {
        if (sbjs == null) {
            return null;
        }
        return sbjs.stream().map(Subject::getValue).collect(Collectors.toList());
    }

    @Named("mapPolicies")
    default List<String> mapPolicies(List<CodeLabel> policies ) {
        if (policies == null) {
            return null;
        }
        return policies.stream().map(CodeLabel::getLabel).collect(Collectors.toList());
    }


    @Named("mapJournal")
    @Mapping(target = "conferencePlace", source = "conferenceplace")
    @Mapping(target = "conferenceDate", source = "conferencedate")
    Container mapJournal(Journal journal );


    @Named("mapIndicators")
    default Indicator mapIndicators(List<Measure> measureList) {
        return Utils.mapIndicators(measureList);
    }


}
