package eu.openaire.api.mappers.response;

import eu.openaire.api.dto.response.SearchHeader;
import eu.openaire.api.dto.response.SearchHeaderDebug;
import eu.openaire.api.solr.SolrQueryParams;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.Optional;

@Mapper(componentModel = "spring")
public interface ResponseHeaderMapper {

	@Mapping(target = "numFound", expression = "java( Long.valueOf(queryResponse.getResults().getNumFound()) )")
	@Mapping(target = "maxScore", source = "queryResponse.results.maxScore")
	@Mapping(target = "page", source = "page")
	@Mapping(target = "pageSize", source = "pageSize")
	@Mapping(target = "nextCursor", source = "queryResponse.nextCursorMark")
	@Mapping(target = "queryTime", expression = "java( (int) queryResponse.getHeader().get(\"QTime\") )")
	SearchHeader toSearchHeader(QueryResponse queryResponse, SolrQueryParams solrQueryParams, int page, int pageSize);

	@AfterMapping
	default void removePage(@MappingTarget SearchHeader searchHeader) {
		if (searchHeader.getNextCursor() != null) {
			searchHeader.setPage(null);
		}
	}

	default SearchHeaderDebug mapDebug(QueryResponse queryResponse, SolrQueryParams solrQueryParams, boolean debugQuery) {
		if (!debugQuery) {
			return null;
		}

		SearchHeaderDebug searchHeaderDebug = new SearchHeaderDebug();

		// set the query params
		searchHeaderDebug.setQueryParams(solrQueryParams);

		// set the debug info
		var debugMap = queryResponse.getDebugMap();
		if (debugMap != null) {

			searchHeaderDebug.setParsedQuery(
					Optional.ofNullable(queryResponse.getDebugMap().get("parsedquery"))
							.map(Object::toString)
							.orElse(null)
			);

			searchHeaderDebug.setParsedFilterQuery(
					Optional.ofNullable(queryResponse.getDebugMap().get("parsed_filter_queries"))
							.map(Object::toString)
							.orElse(null)
			);

		}

		return searchHeaderDebug;
	}

}