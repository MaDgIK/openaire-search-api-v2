package eu.openaire.api.mappers.response.entities;

import eu.dnetlib.dhp.oa.model.BipIndicators;
import eu.dnetlib.dhp.oa.model.Indicator;
import eu.dnetlib.dhp.oa.model.UsageCounts;
import eu.dnetlib.dhp.schema.solr.Measure;

import java.util.List;

public class Utils {

    public static Indicator mapIndicators(List<Measure> measureList) {
        if (measureList == null) {
            return null;
        }

        Indicator i = new Indicator();
        for (Measure m : measureList) {
            switch (m.getId()) {
                case Constants.USAGE_COUNT_DOWNLOADS:
                    getUsageCounts(i).setDownloads(Integer.parseInt(m.getUnit().get(0).getLabel()));
                    break;
                case Constants.USAGE_COUNT_VIEWS:
                    getUsageCounts(i).setViews(Integer.parseInt(m.getUnit().get(0).getLabel()));
                    break;
                case Constants.BIP_INFLUENCE:
                    m.getUnit().forEach(u -> {
                        if (u.getCode().equals("class")) {
                            getImpactMeasure(i).setInfluenceClass(u.getLabel());
                        }
                        if (u.getCode().equals("score")) {
                            getImpactMeasure(i).setInfluence(Double.parseDouble(u.getLabel()));
                        }
                    });
                    break;
                case Constants.BIP_POPULARITY:
                    m.getUnit().forEach(u -> {
                        if (u.getCode().equals("class")) {
                            getImpactMeasure(i).setPopularityClass(u.getLabel());
                        }
                        if (u.getCode().equals("score")) {
                            getImpactMeasure(i).setPopularity(Double.parseDouble(u.getLabel()));
                        }
                    });
                    break;
                case Constants.BIP_INFLUENCE_ALT:
                    m.getUnit().forEach(u -> {
                        if (u.getCode().equals("class")) {
                            getImpactMeasure(i).setCitationClass(u.getLabel());
                        }
                        if (u.getCode().equals("score")) {
                            getImpactMeasure(i).setCitationCount(Double.parseDouble(u.getLabel()));
                        }
                    });
                    break;
                case Constants.BIP_POPULARITY_ALT:
                    break;
                case Constants.BIP_IMPULSE:
                    m.getUnit().forEach(u -> {
                        if (u.getCode().equals("class")) {
                            getImpactMeasure(i).setImpulseClass(u.getLabel());
                        }
                        if (u.getCode().equals("score")) {
                            getImpactMeasure(i).setImpulse(Double.parseDouble(u.getLabel()));
                        }
                    });
                    break;
                default:
                    throw new RuntimeException("No mapping found for indicator " + m.getId());

            }
        }

        return i;

    }

    private static UsageCounts getUsageCounts(Indicator i) {
        if (i.getUsageCounts() == null) {
            i.setUsageCounts(new UsageCounts());
        }
        return i.getUsageCounts();
    }

    private static BipIndicators getImpactMeasure(Indicator i) {
        if (i.getCitationImpact() == null) {
            i.setCitationImpact(new BipIndicators());
        }
        return i.getCitationImpact();
    }


}
