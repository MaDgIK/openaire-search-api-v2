package eu.openaire.api.mappers.response.entities;

import eu.dnetlib.dhp.oa.model.APC;
import eu.dnetlib.dhp.oa.model.AccessRight;
import eu.dnetlib.dhp.oa.model.AlternateIdentifier;
import eu.dnetlib.dhp.oa.model.AuthorPid;
import eu.dnetlib.dhp.oa.model.AuthorPidSchemeValue;
import eu.dnetlib.dhp.oa.model.Container;
import eu.dnetlib.dhp.oa.model.OpenAccessRoute;
import eu.dnetlib.dhp.oa.model.Indicator;
import eu.dnetlib.dhp.oa.model.ResultCountry;
import eu.dnetlib.dhp.oa.model.ResultPid;
import eu.dnetlib.dhp.oa.model.SubjectSchemeValue;
import eu.dnetlib.dhp.oa.model.graph.GraphResult;
import eu.dnetlib.dhp.schema.common.ModelConstants;
import eu.dnetlib.dhp.schema.solr.Author;
import eu.dnetlib.dhp.schema.solr.BestAccessRight;
import eu.dnetlib.dhp.schema.solr.Country;
import eu.dnetlib.dhp.schema.solr.Instance;
import eu.dnetlib.dhp.schema.solr.Journal;
import eu.dnetlib.dhp.schema.solr.Language;
import eu.dnetlib.dhp.schema.solr.Measure;
import eu.dnetlib.dhp.schema.solr.OpenAccessColor;
import eu.dnetlib.dhp.schema.solr.Pid;
import eu.dnetlib.dhp.schema.solr.SolrRecord;
import eu.dnetlib.dhp.schema.solr.Subject;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface ResearchProductMapper {


//From the model of the dump we miss the fulltext and the eoscIFGuidelines, otherTitles, transformativeAgreement. We need to consider if we want to include them in the dump
    @Mapping(target = "pids", expression = "java(mapPids(payload.getPid()) )")
    @Mapping(target = "indicators", expression = "java(mapIndicators(payload.getMeasures()))")
    @Mapping(target = "id", source = "payload.header.id")
    @Mapping(target = "originalIds", source = "payload.header.originalId")
    @Mapping(target = "type", source = "payload.result.resulttype")
    @Mapping(target = "authors", expression = "java(mapAuthors(payload.getResult().getAuthor()))")
    @Mapping(target = "subjects", expression = "java(mapSubjects(payload.getResult().getSubject()) )")
    @Mapping(target = "language", expression = "java(mapLanguage(payload.getResult().getLanguage()))")
    @Mapping(target = "countries", expression = "java(mapCountries(payload.getResult().getCountry()))")
    @Mapping(target = "mainTitle", source = "payload.result.maintitle")
    @Mapping(target = "descriptions", source = "payload.result.description")
    @Mapping(target = "publicationDate", source = "payload.result.publicationdate")
    @Mapping(target = "publisher", source = "payload.result.publisher")
    @Mapping(target = "embargoEndDate", source = "payload.result.embargoenddate")
    @Mapping(target = "sources", source = "payload.result.source")
    @Mapping(target = "formats", source = "payload.result.format")
    @Mapping(target = "contributors", source = "payload.result.contributor")
    @Mapping(target = "coverages", source = "payload.result.coverage")
    @Mapping(target = "bestAccessRight", expression = "java(mapBestAccessRight(payload.getResult().getBestaccessright()))")
    @Mapping(target = "container", expression = "java(mapJournal(payload.getResult().getJournal()) )")
    @Mapping(target = "documentationUrls", source = "payload.result.documentationUrl")
    @Mapping(target = "codeRepositoryUrl", source = "payload.result.codeRepositoryUrl")
    @Mapping(target = "programmingLanguage", source = "payload.result.programmingLanguage")
    @Mapping(target = "contactPeople", source = "payload.result.contactperson")
    @Mapping(target = "contactGroups", source = "payload.result.contactgroup")
    @Mapping(target = "tools", source = "payload.result.tool")
    @Mapping(target = "size", source = "payload.result.size")
    @Mapping(target = "version", source = "payload.result.version")
    @Mapping(target = "isGreen", source = "payload.result.isGreen")
    @Mapping(target = "openAccessColor", expression = "java(mapOpenAccessColor(payload.getResult().getOpenAccessColor()))")
    @Mapping(target = "isInDiamondJournal", source = "payload.result.isInDiamondJournal")
    @Mapping(target = "publiclyFunded", source = "payload.result.publiclyFunded")
    @Mapping(target = "instances", expression = "java(mapInstances(payload.getResult().getInstance()))")
    GraphResult toGraphResult(SolrRecord payload);


    @Mapping(target = "scheme", source = "typeCode")
    ResultPid mapPid(Pid pid);

    @Named("mapIndicators")
    default Indicator mapIndicators(List<Measure> measureList) {
        return Utils.mapIndicators(measureList);
    }

    @Named("mapPids")
    default List<ResultPid> mapPids(List<Pid> pids ) {
        if (pids == null) {
            return null;
        }
        return pids.stream().map(this::mapPid).collect(Collectors.toList());
    }

    @Named("mapAltIds")
    default List<AlternateIdentifier> mapAltIds(List<Pid> altIdentifiers) {
        if (altIdentifiers == null) {
            return null;
        }
        return altIdentifiers.stream().map(this::mapAltId).collect(Collectors.toList());
    }

    @Mapping(target = "scheme", source = "typeCode")
    AlternateIdentifier mapAltId(Pid altId);

    @Named("mapJournal")
    @Mapping(target = "conferencePlace", source = "conferenceplace")
    @Mapping(target = "conferenceDate", source = "conferencedate")
    Container mapJournal(Journal journal );

    @Named("mapSubjects")
    default List<eu.dnetlib.dhp.oa.model.Subject> mapSubjects(List<Subject> sbjs ) {
        if (sbjs == null) {
            return null;
        }
        return sbjs.stream().map(this::mapSubject).collect(Collectors.toList());
    }

    @Mapping(target = "subject", expression = "java( mapSubjectSchemeValue(subject))")
    eu.dnetlib.dhp.oa.model.Subject mapSubject(Subject subject);

    @Named("mapSubjectSchemeValue")
    @Mapping(target = "scheme", source = "typeCode")
    SubjectSchemeValue mapSubjectSchemeValue(Subject subject);

    @Named("mapCountries")
    default List<ResultCountry> mapCountries(List<Country> countryList) {
        if (countryList == null) {
            return null;
        }
        return countryList.stream().map(this::mapCountry).collect(Collectors.toList());
    }

    ResultCountry mapCountry(Country country);

    @Named("mapLanguage")
    eu.dnetlib.dhp.oa.model.Language mapLanguage(Language language);

    @Named("mapOpenAccessColor")
    eu.dnetlib.dhp.oa.model.OpenAccessColor mapOpenAccessColor(OpenAccessColor openAccessColor);

    @Named("mapAuthor")
    default List<eu.dnetlib.dhp.oa.model.Author> mapAuthors(List<Author> authorsList) {
        if (authorsList == null) {
            return null;
        }
        return authorsList.stream().map(this::mapAuthor).collect(Collectors.toList());
   }

   @Mapping(target = "fullName", source = "author.fullname")
   @Mapping(target = "pid", expression =  "java(mapAuthorPid(author.getPid()))")
   eu.dnetlib.dhp.oa.model.Author mapAuthor(Author author);

    @Named("mapAuthorPid")
    default AuthorPid mapAuthorPid(List<Pid> authorPidList) {
        if (authorPidList == null) {
            return null;
        }
        List<Pid> orcid = authorPidList
                .stream()
                .filter(ap -> ModelConstants.ORCID.equals(ap.getTypeCode()))
                .toList();
        if (orcid.size() == 1) {
            return getAuthorPid(orcid.get(0));
        }
        if (orcid.size() > 1 ) {
            return null;
        }
        orcid = authorPidList
                .stream()
                .filter(ap -> ModelConstants.ORCID_PENDING.equals(ap.getTypeCode()))
                .toList();
        if (orcid.size() == 1) {
            return getAuthorPid(orcid.get(0));
        }

        return null;
    }

    @Mapping(target = "id", expression = "java(mapAuthorPidSchemeValue(pid))")
    AuthorPid getAuthorPid(Pid pid);

    @Named("mapAuthorPidSchemeValue")
    @Mapping(target = "scheme", source = "pid.typeCode")
    AuthorPidSchemeValue mapAuthorPidSchemeValue (Pid pid);

    @Named("mapBestAccessRight")
    default eu.dnetlib.dhp.oa.model.BestAccessRight mapBestAccessRight(BestAccessRight bestAccessRight) {
        if (bestAccessRight == null) {
            return null;
        }
        if (Constants.ACCESS_RIGHTS_COAR_MAP.containsKey(bestAccessRight.getCode())) {
            String code = Constants.ACCESS_RIGHTS_COAR_MAP.get(bestAccessRight.getCode());
            return
                            eu.dnetlib.dhp.oa.model.BestAccessRight
                                    .newInstance(
                                            code,
                                            Constants.COAR_CODE_LABEL_MAP.get(code),
                                            Constants.COAR_ACCESS_RIGHT_SCHEMA);
        }
        return null;
    }

    @Named("mapInstances")
    default List<eu.dnetlib.dhp.oa.model.Instance> mapInstances(List<Instance> instanceList) {
        if (instanceList == null) {
            return null;
        }
        return instanceList.stream().map(this::mapInstance).collect(Collectors.toList());
    }


    @Mapping(target = "type", source = "instance.instancetype")
    @Mapping(target = "urls", source = "instance.url")
    @Mapping(target = "publicationDate", source = "instance.dateofacceptance")
    @Mapping(target = "pids", expression = "java( mapPids(instance.getPid()) )")
    @Mapping(target = "alternateIdentifiers", expression = "java( mapAltIds(instance.getAlternateIdentifier()) )")
    @Mapping(target = "accessRight", expression = "java(mapAccessRight(instance.getAccessright()))")
    @Mapping(target = "articleProcessingCharge", expression = "java(mapAPC(instance.getProcessingcharges()))")
    eu.dnetlib.dhp.oa.model.Instance mapInstance(Instance instance);

    @Named("mapAccessRight")
    default AccessRight mapAccessRight(eu.dnetlib.dhp.schema.solr.AccessRight accessright) {
        if (accessright == null) {
            return null;
        }
        AccessRight ar = new AccessRight();
        if (Constants.ACCESS_RIGHTS_COAR_MAP.containsKey(accessright.getLabel())) {
            String code = Constants.ACCESS_RIGHTS_COAR_MAP.get(accessright.getLabel());
            ar.setCode(code);
            ar.setLabel(Constants.COAR_CODE_LABEL_MAP.get(code));
            ar.setScheme(Constants.COAR_ACCESS_RIGHT_SCHEMA);
            ar.setOpenAccessRoute(mapOpenAccessRoute(accessright.getOpenAccessRoute()));
            return ar;
        }
        return null;
    }


    @Named("mapAPC")
    APC mapAPC(eu.dnetlib.dhp.schema.solr.APC apc);

    @Named("mapOpenAccessRoute")
    OpenAccessRoute mapOpenAccessRoute (eu.dnetlib.dhp.schema.solr.OpenAccessRoute openAccessRoute);
}
