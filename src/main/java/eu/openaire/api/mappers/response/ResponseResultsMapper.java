package eu.openaire.api.mappers.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dnetlib.dhp.schema.solr.SolrRecord;
import eu.openaire.api.solr.SolrResponseBean;
import lombok.RequiredArgsConstructor;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ResponseResultsMapper {

	private final ObjectMapper objectMapper;

	public <T> List<T> toSearchResults(QueryResponse queryResponse, Function<SolrRecord, T> mapFunction) throws RuntimeException {
		return queryResponse.getBeans(SolrResponseBean.class)
				.stream()
				.map(SolrResponseBean::getJsonField)
				.map(json -> {
					try {
						return objectMapper.readValue(json, SolrRecord.class);
					} catch (JsonProcessingException e) {
						throw new RuntimeException("Error deserializing JSON", e);
					}
				})
				.map(mapFunction)
				.collect(Collectors.toList());
	}

	public <T> T toSingleResult(String jsonPayload, Function<SolrRecord, T> mapFunction) throws RuntimeException {
		try {

			// Deserialize the JSON field to SolrRecord
			SolrRecord solrRecord = objectMapper.readValue(jsonPayload, SolrRecord.class);

			// Apply the mapFunction to transform SolrRecord to T
			return mapFunction.apply(solrRecord);

		} catch (JsonProcessingException e) {
			throw new RuntimeException("Error deserializing JSON", e);
		}
	}
}
