package eu.openaire.api.mappers.response.entities;

import eu.dnetlib.dhp.oa.model.Country;
import eu.dnetlib.dhp.oa.model.Indicator;
import eu.dnetlib.dhp.oa.model.graph.Organization;
import eu.dnetlib.dhp.oa.model.graph.OrganizationPid;
import eu.dnetlib.dhp.schema.solr.CodeLabel;
import eu.dnetlib.dhp.schema.solr.Measure;
import eu.dnetlib.dhp.schema.solr.Pid;
import eu.dnetlib.dhp.schema.solr.SolrRecord;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring")
public interface OrganizationMapper {

    @Mapping(target = "legalShortName", source = "payload.organization.legalshortname")
    @Mapping(target = "legalName", source = "payload.organization.legalname")
    @Mapping(target = "country", expression = "java( mapCountry(payload.getOrganization().getCountry()))")
    @Mapping(target = "alternativeNames", source = "payload.organization.alternativeNames")
    @Mapping(target = "websiteUrl", source = "payload.organization.websiteurl")
    @Mapping(target = "id", source = "payload.header.id")
    @Mapping(target = "pids", expression = "java( mapPids(payload.getPid()) )")
    Organization toGraphOrganization(SolrRecord payload);

    @Named("mapCountry")
    Country mapCountry(CodeLabel codeLabel) ;

    @Mapping(target = "scheme", source = "typeCode")
    OrganizationPid mapPid(Pid pid);

    @Named("mapPids")
    default List<OrganizationPid> mapPids(List<Pid> pids ) {
        if (pids == null) {
            return null;
        }
        return pids.stream().map(this::mapPid).collect(Collectors.toList());
    }

    @Named("mapIndicators")
    default Indicator mapIndicators(List<Measure> measureList) {
        return Utils.mapIndicators(measureList);
    }

}
