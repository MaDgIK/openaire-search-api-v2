package eu.openaire.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class OpenaireRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenaireRestApiApplication.class, args);
	}

}
