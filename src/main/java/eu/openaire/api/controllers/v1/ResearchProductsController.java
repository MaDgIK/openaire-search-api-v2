package eu.openaire.api.controllers.v1;

import eu.dnetlib.dhp.oa.model.graph.GraphResult;
import eu.dnetlib.dhp.schema.sx.api.model.v2.PageResultType;
import eu.openaire.api.dto.request.v1.ResearchProductsRequest;
import eu.openaire.api.dto.request.validators.AllowableFieldsValidator;
import eu.openaire.api.dto.request.validators.PaginationValidator;
import eu.openaire.api.dto.request.validators.ResearchProductsRequestValidator;
import eu.openaire.api.dto.request.validators.Utils;
import eu.openaire.api.dto.response.entities.ResearchProductsSearchResponse;
import eu.openaire.api.dto.response.SearchResponse;
import eu.openaire.api.errors.ErrorResponse;
import eu.openaire.api.errors.exceptions.BadRequestException;
import eu.openaire.api.services.ResearchProductService;
import eu.openaire.api.services.ScholixService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/researchProducts")
@RequiredArgsConstructor
@Tag(name = "Research products", description = "API endpoints to explore research products")
public class ResearchProductsController {
	private final ResearchProductService researchProductService;
	private final ScholixService scholixService;

	// common validator to check api allowable fields
	private final AllowableFieldsValidator allowableFieldsValidator;

	// common validator to check pagination parameters
	private final PaginationValidator paginationValidator;

	// request object specific validator
	private final ResearchProductsRequestValidator researchProductsRequestValidator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(allowableFieldsValidator, researchProductsRequestValidator, paginationValidator);
	}

	@Operation(
			summary = "Retrieve a research product by id",
			description = "Get a research product object by specifying its id.")
	@ApiResponses({
			@ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = GraphResult.class), mediaType = "application/json") }),
			@ApiResponse(responseCode = "404", content = { @Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "application/json") }),
			@ApiResponse(responseCode = "500", content = { @Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "application/json") })})
	@GetMapping(value = "/{id}")
	public GraphResult getById(@PathVariable @Parameter(description = "The OpenAIRE id of the research product") String id) {
		return researchProductService.getById(id);
	}

	@Operation(
			summary = "Search for research products",
			description = "Explore research products exploiting various filter parameters")
	@ApiResponses({
			@ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = ResearchProductsSearchResponse.class), mediaType = "application/json") }),
			@ApiResponse(responseCode = "404", content = { @Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "application/json") }),
			@ApiResponse(responseCode = "500", content = { @Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "application/json") })})
	@GetMapping(value = "")
	public SearchResponse<GraphResult> search(@Valid @ParameterObject final ResearchProductsRequest request, BindingResult validationResult) {
		if (validationResult.hasErrors()) {
			throw new BadRequestException(Utils.getErrorMessage(validationResult.getAllErrors()));
		}

		return researchProductService.search(request);
	}

	@Operation(
			summary = "Retrieve scholix links",
			description = "Retrieve scholix links"
	)
	@ApiResponses({
			@ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = PageResultType.class), mediaType = "application/json") }),
			@ApiResponse(responseCode = "404", content = { @Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "application/json") }),
			@ApiResponse(responseCode = "500", content = { @Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "application/json") })
	})
	@GetMapping("/links")
	public PageResultType getLinks(@RequestParam @Parameter(description = "sourcePid") String sourcePid,
								   @RequestParam(required = false) @Parameter(description = "targetPid") String targetPid,
								   @RequestParam(required = false) @Parameter(description = "targetPublisher") String targetPublisher,
								   @RequestParam(required = false) @Parameter(description = "targetType") String targetType,
								   @RequestParam(required = false) @Parameter(description = "sourcePublisher") String sourcePublisher,
								   @RequestParam(required = false) @Parameter(description = "sourceType") String sourceType,
								   @RequestParam(required = false) @Parameter(description = "relation") String relation,
								   @RequestParam(required = false) @Parameter(description = "relationDate") String relationDate,
								   @RequestParam(defaultValue = "0") @Parameter(description = "Page number of the results") int page) {

		return scholixService.getLinks(sourcePid,
				targetPid,
				targetPublisher,
				targetType,
				sourcePublisher,
				sourceType,
				relation,
				relationDate,
				page);
	}
}
