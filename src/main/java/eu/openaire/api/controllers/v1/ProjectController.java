package eu.openaire.api.controllers.v1;

import eu.dnetlib.dhp.oa.model.graph.Project;
import eu.openaire.api.dto.request.v1.ProjectRequest;
import eu.openaire.api.dto.request.validators.AllowableFieldsValidator;
import eu.openaire.api.dto.request.validators.PaginationValidator;
import eu.openaire.api.dto.request.validators.Utils;
import eu.openaire.api.dto.response.SearchResponse;
import eu.openaire.api.dto.response.entities.ProjectSearchResponse;
import eu.openaire.api.errors.ErrorResponse;
import eu.openaire.api.errors.exceptions.BadRequestException;
import eu.openaire.api.services.ProjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/projects")
@RequiredArgsConstructor
@Tag(name = "Projects", description = "API endpoints to explore projects")
public class ProjectController {
    private final ProjectService projectService;

    // common validator to check api allowable fields
    private final AllowableFieldsValidator allowableFieldsValidator;

    // common validator to check pagination parameters
    private final PaginationValidator paginationValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(allowableFieldsValidator, paginationValidator);
    }

    @Operation(
        summary = "Retrieve a project by id",
        description = "Get a project object by specifying its id.")
    @ApiResponses({
        @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = ProjectSearchResponse.class), mediaType = "application/json") }),
        @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "application/json") }),
        @ApiResponse(responseCode = "500", content = { @Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "application/json") })})
    @GetMapping(value = "/{id}")
    public Project getById(@PathVariable @Parameter(description = "The OpenAIRE id of the project") String id) {
        return projectService.getById(id);
    }

    @Operation(
        summary = "Search for projects",
        description = "Explore projects exploiting various filter parameters")
    @ApiResponses({
        @ApiResponse(responseCode = "200", content = { @Content(schema = @Schema(implementation = SearchResponse.class), mediaType = "application/json") }),
        @ApiResponse(responseCode = "404", content = { @Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "application/json") }),
        @ApiResponse(responseCode = "500", content = { @Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "application/json") })})
    @GetMapping(value = "")
    public SearchResponse<Project> search(@Valid @ParameterObject final ProjectRequest request, BindingResult validationResult) {
        if (validationResult.hasErrors()) {
            throw new BadRequestException(Utils.getErrorMessage(validationResult.getAllErrors()));
        }

        return projectService.search(request);
    }
}
