package eu.openaire.api.mappers.response;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dnetlib.dhp.oa.model.Container;
import eu.dnetlib.dhp.oa.model.graph.Datasource;
import eu.dnetlib.dhp.schema.solr.SolrRecord;
import eu.openaire.api.mappers.response.entities.DatasourceMapper;
import eu.openaire.api.mappers.response.entities.DatasourceMapperImpl;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DatasourceMapper.class})
public class DatasourceMapperTest {

    @Test
    public void testDatasourceMapperImpl() throws IOException {
        SolrRecord solrRecord = new ObjectMapper().readValue(IOUtils
                .toString(
                        Objects.requireNonNull(DatasourceMapperTest.class
                                .getResourceAsStream(
                                        "/eu/openaire/api/mappers/response/SolrDatasource.json"))), SolrRecord.class);

        var datasourceMapper = new DatasourceMapperImpl();

        Datasource graphDatasource = datasourceMapper.toGraphDatasource(solrRecord);

        Assertions.assertEquals("doajarticles::614fdb5f82725ed3f8834ae90b9a0212", graphDatasource.getId());
        Assertions.assertEquals(2, graphDatasource.getOriginalIds().size());
        Assertions.assertTrue(graphDatasource.getOriginalIds().containsAll(Arrays.asList("doajarticles::2196-8403", "issn___print::2196-8403")));
        Assertions.assertEquals("Journal", graphDatasource.getType().getValue());
        Assertions.assertEquals("pubsrepository::journal", graphDatasource.getType().getScheme());
        Assertions.assertEquals("collected from a compatible aggregator", graphDatasource.getOpenaireCompatibility());
        Assertions.assertEquals("Convivium", graphDatasource.getOfficialName());
        Assertions.assertEquals("Convivium", graphDatasource.getEnglishName());
        Assertions.assertEquals(1, graphDatasource.getSubjects().size());
        Assertions.assertEquals("Language and Literature: German literature | Language and Literature: Philology. Linguistics", graphDatasource.getSubjects().get(0));
        Assertions.assertNotNull(graphDatasource.getJournal());
        Container journal = graphDatasource.getJournal();

        Assertions.assertEquals("Convivium", journal.getName());
        Assertions.assertEquals("2196-8403", journal.getIssnPrinted());
        Assertions.assertEquals("2657-6252", journal.getIssnOnline());
        Assertions.assertNull(journal.getIssnLinking());
        Assertions.assertNull(journal.getIss());
        Assertions.assertNull(journal.getEdition());
        Assertions.assertNull(journal.getConferenceDate());
        Assertions.assertNull(journal.getConferencePlace());
        Assertions.assertNull(journal.getEp());
        Assertions.assertNull(journal.getSp());
        Assertions.assertNull(journal.getVol());

    }

}
