package eu.openaire.api.mappers.response;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dnetlib.dhp.oa.model.Author;
import eu.dnetlib.dhp.oa.model.Instance;
import eu.dnetlib.dhp.oa.model.graph.GraphResult;
import eu.dnetlib.dhp.schema.solr.SolrRecord;
import eu.openaire.api.mappers.response.entities.Constants;
import eu.openaire.api.mappers.response.entities.ResearchProductMapper;
import eu.openaire.api.mappers.response.entities.ResearchProductMapperImpl;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ResearchProductMapper.class})
public class ResultProductMapperTest {

    @Test
    public void resultMapperImplTest() throws IOException {
        SolrRecord solrRecord = new ObjectMapper().readValue(IOUtils
                .toString(
                        Objects.requireNonNull(ResultProductMapperTest.class
                                .getResourceAsStream(
                                        "/eu/openaire/api/mappers/response/SolrResult.json"))), SolrRecord.class);

        var resultMapper = new ResearchProductMapperImpl();

        GraphResult graphResult = resultMapper.toGraphResult(solrRecord);

        Assertions.assertEquals("CSC_________::207f2db8a5174e57db3715909a2ffa76", graphResult.getId());
        Assertions.assertEquals(2, graphResult.getOriginalIds().size());
        Assertions.assertTrue(graphResult.getOriginalIds().contains("CSC_________::207f2db8a5174e57db3715909a2ffa76") &&
                graphResult.getOriginalIds().contains("oai:virta-jtp.csc.fi:Publications/0389383002"));
        Assertions.assertEquals("publication", graphResult.getType());
        Assertions.assertEquals(11, graphResult.getAuthors().size());

        List<String> fullNames = graphResult.getAuthors().stream().map(eu.dnetlib.dhp.oa.model.Author::getFullName).toList();

        Assertions.assertTrue(fullNames.containsAll(Arrays.asList("Ahola, Jari","Alahuhta, Petteri","Kaasinen, Eija","Korhonen, Ilkka","Plomp, Johan","Ari","Virtanen","Rentto, Katja","Pakanen, Jouko","Lappalainen, Veijo","Laikari, Arto")));

        Author author = graphResult.getAuthors().stream().filter(a -> a.getFullName().equals("Ahola, Jari")).findFirst().get();
        Assertions.assertEquals("Jari", author.getName());
        Assertions.assertEquals("Ahola", author.getSurname());
        Assertions.assertEquals(1, author.getRank());
        Assertions.assertNull(author.getPid());

        author = graphResult.getAuthors().stream().filter(a -> a.getFullName().equals("Virtanen")).findFirst().get();
        Assertions.assertNull(author.getPid());
        Assertions.assertNull(author.getName());
        Assertions.assertNull(author.getSurname());
        Assertions.assertEquals(9, author.getRank());

        author = graphResult.getAuthors().stream().filter(a -> a.getFullName().equals("Plomp, Johan")).findFirst().get();
        Assertions.assertEquals("Johan", author.getName());
        Assertions.assertEquals("Plomp", author.getSurname());
        Assertions.assertEquals(11, author.getRank());
        Assertions.assertNotNull(author.getPid());
        Assertions.assertEquals("orcid_pending", author.getPid().getId().getScheme());
        Assertions.assertEquals("0000-0002-0425-5364", author.getPid().getId().getValue());

        Assertions.assertEquals("Smart human environments",graphResult.getMainTitle());
        Assertions.assertNull(graphResult.getSubTitle());
        Assertions.assertEquals("2002-01-01", graphResult.getPublicationDate());
        Assertions.assertEquals(Constants.ACCESS_RIGHTS_COAR_MAP.get("CLOSED"), graphResult.getBestAccessRight().getCode());
        Assertions.assertEquals(Constants.COAR_CODE_LABEL_MAP.get(Constants.C14CB), graphResult.getBestAccessRight().getLabel());
        Assertions.assertEquals(Constants.COAR_ACCESS_RIGHT_SCHEMA, graphResult.getBestAccessRight().getScheme());

        Assertions.assertFalse(graphResult.getPubliclyFunded());
        Assertions.assertFalse(graphResult.getIsGreen());
        Assertions.assertFalse(graphResult.getIsInDiamondJournal());
        Assertions.assertEquals(1, graphResult.getInstances().size());

        Instance instance = graphResult.getInstances().get(0);
        Assertions.assertEquals(Constants.COAR_ACCESS_RIGHT_SCHEMA, instance.getAccessRight().getScheme());
        Assertions.assertEquals(Constants.ACCESS_RIGHTS_COAR_MAP.get("CLOSED"), instance.getAccessRight().getCode());
        Assertions.assertEquals(Constants.COAR_CODE_LABEL_MAP.get(Constants.C14CB), instance.getAccessRight().getLabel());
        Assertions.assertNull(instance.getAccessRight().getOpenAccessRoute());

        Assertions.assertEquals("0013", instance.getType());
        Assertions.assertEquals(1, instance.getUrls().size());
        Assertions.assertTrue(instance.getUrls().contains("http://juuli.fi/Record/0389383002"));
        Assertions.assertEquals("0002", instance.getRefereed());

    }

}
