# OpenAIRE Graph API

Welcome to the OpenAIRE Graph API code repository! 

This repository houses the latest implementation of our OpenAIRE Graph API, 
designed to facilitate seamless access to metadata records of research products (i.e., publications, data, software, other research products), and projects
within the OpenAIRE Graph. 

For more information, please take a look at our [documentation page](https://graph.openaire.eu/docs/next/apis/graph-api/).

# Getting Started

### Reference for developers
- This project is built with Java 21; tested using OpenJDK 21.0.2.

- Before starting the application, please load env variables from `.env` file.
If you are using IntelliJ, you can do that by navigating to `Run -> Edit Configurations -> Modify Options`, check `Environment variables`
under `Operating Systems`, and append the env variables with a semicolon separator.


### Docker support

Create a docker image using:

```shell
# building the jar file
mvn install 

# building the actual docker image
docker build -t openaire-graph-api:latest . 
```

Start a docker container as follows: 

```shell
# load required env variables
source .env

# start docker container
docker run -it -p 8080:8080 openaire-graph-api:latest .
```

or using docker compose:
```shell
# adjust env variables in docker-compose.yml

# start container using docker compose 
docker compose up -d
```

In both case, Swagger should be accessible at `${OPENAPI_SERVER_BASE_URL}/graph/swagger-ui/index.html#/`
or http://localhost:8080/graph/swagger-ui/index.html#/ if you are running it on localhost.


