FROM maven:3.9.6-eclipse-temurin-21-alpine AS MAVEN_BUILD

MAINTAINER OpenAIRE AMKE

WORKDIR /build

COPY pom.xml .

RUN mvn dependency:go-offline

COPY src ./src

RUN mvn clean package -DskipTests=true


FROM eclipse-temurin:21-jdk-alpine

RUN mkdir /app

WORKDIR /app

COPY --from=MAVEN_BUILD /build/target/*.jar /app/app.jar

VOLUME /tmp

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app/app.jar"]